# pdool-server

#### 介绍
这是一个游戏服务器，是项目向的websocket一个服务器，
主要目标是完成当前项目进行开发，后期通过抽象，优化成为一个通用的服务器架构


#### 软件架构
使用的技术：
docker
springboot，dubbo，protobuf

数据库层面暂时没定：MongoDB，redis 或者mysql都有可能

模块介绍

client  模拟协议发送

Gate 网关服，获取game链接信息

Game 游戏服

room 房间服

#### 安装教程

不需要安装，下载下来之后，运行就行

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技


