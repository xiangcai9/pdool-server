package com.pdool.game.core.msg;

import com.google.protobuf.GeneratedMessageV3;

import java.io.IOException;

public interface IHandler <IN extends GeneratedMessageV3, OUT extends GeneratedMessageV3>{

    OUT handle(Long roleId, IN in) throws IOException;
}
