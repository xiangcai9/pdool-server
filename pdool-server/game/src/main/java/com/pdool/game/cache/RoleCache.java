package com.pdool.game.cache;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdool.common.constants.Constant;
import com.pdool.common.entity.RoleInfoEntity;
import com.pdool.common.entity.RoleNumInfoEntity;
import com.pdool.common.mapper.impl.RoleInfoDaoImpl;
import com.pdool.common.mapper.impl.RoleNumInfoDaoImpl;
import com.pdool.game.logic.enums.NumTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 角色基础信息缓存
 */
@Slf4j
@Service
public class RoleCache {
    @Resource
    RoleInfoDaoImpl playerRepository;
    @Resource
    RoleNumInfoDaoImpl roleNumInfoRepository;

    @Cacheable(value = "user", key = "#roleId")
    public RoleInfoEntity queryRoleOne(long roleId) {
        QueryWrapper<RoleInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId); //name不为空的用户
        return playerRepository.getOne(wrapper);
    }

    @CachePut(value = "user", key = "#roleInfoEntity.roleId")
    public void updateUser(RoleInfoEntity roleInfoEntity) {
        playerRepository.saveOrUpdate(roleInfoEntity);
    }

    @Cacheable(value = "user_num", key = "#roleId")
    public Map<Integer, RoleNumInfoEntity> queryRoleNumInfo(long roleId) {
        QueryWrapper<RoleNumInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId); //name不为空的用户
        List<RoleNumInfoEntity> all = roleNumInfoRepository.list(wrapper);
        Map<Integer, RoleNumInfoEntity> collect = new HashMap<>();
        if (all != null) {
            for (RoleNumInfoEntity roleNumInfoEntity : all) {
                collect.put(roleNumInfoEntity.getNumType(), roleNumInfoEntity);
            }
        }
        return collect;
    }


    @Transactional
    @CachePut(value = "user_num", key = "#roleId")
    public Map<Integer, RoleNumInfoEntity> updateRoleNum(long roleId, Map<Integer, RoleNumInfoEntity> numMap, NumTypeEnum numType, Long value, String extend) {
        RoleNumInfoEntity orDefault = numMap.getOrDefault(numType.getNumType(), new RoleNumInfoEntity());
        orDefault.setRoleId(roleId);
        orDefault.setNumType(numType.getNumType());
        if (value != null) {
            orDefault.setNum(orDefault.getNum() == null ? 0L : orDefault.getNum() + value);
        }
        if (StringUtils.hasText(extend)) {
            String s = orDefault.getExtend() + Constant.SPLIT_SEMICOLON + extend;
            orDefault.setExtend(s);
        } else {
            orDefault.setExtend(extend);
        }
        roleNumInfoRepository.saveOrUpdate(orDefault);
        numMap.put(numType.getNumType(), orDefault);
        return numMap;
    }


}
