package com.pdool.game.logic.mgr;

import com.pdool.game.logic.service.RoleService;
import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 用户管理器
 */
@Component
@Data
public class UserMgr implements InitializingBean, ApplicationRunner {
    private static UserMgr instance;
    @Resource
    RoleService roleService;
    //  roleId-session
    private final Map<Long, WebSocketSession> sessionMap = new ConcurrentHashMap<>();
    //  roleId - hb
    private final Map<Long, Long> HBMap = new ConcurrentHashMap<>();
    //  roleId - roleName
    private final Map<Long, String> roleNameMap = new ConcurrentHashMap<>();

    public static UserMgr getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        instance = this;
    }

    @Override
    public void run(ApplicationArguments args) {
        List<Map<String, Object>> maps = roleService.queryAllRoleNames();
        for (Map<String, Object> map : maps) {
            long roleId = (long) map.get("role_id");
            String name = (String) map.get("name");
            this.roleNameMap.put(roleId, name);
        }
    }
}
