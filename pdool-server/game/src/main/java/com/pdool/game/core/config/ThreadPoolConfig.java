package com.pdool.game.core.config;

import com.pdool.common.constants.Constant;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池配置
 */
@Configuration
public class ThreadPoolConfig {


    // 接下来配置一个bean，配置线程池。
    @Bean("logicThreadPool")
    public ThreadPoolTaskExecutor logicThreadPool() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(Constant.LOGIC_THREAD_POOL_CORE_SIZE);// 设置核心线程数
        threadPoolTaskExecutor.setMaxPoolSize(Constant.LOGIC_THREAD_POOL_MAX_SIZE);// 配置最大线程数
        threadPoolTaskExecutor.setQueueCapacity(Constant.LOGIC_THREAD_POOL_QUEUE_SIZE);// 配置队列容量
        threadPoolTaskExecutor.setThreadNamePrefix(Constant.LOGIC_THREAD_POOL_NAME);// 给线程池设置名称
        threadPoolTaskExecutor.setKeepAliveSeconds(Constant.LOGIC_THREAD_POOL_KEEP_LIVE);
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());// 设置任务的拒绝策略
        return threadPoolTaskExecutor;
    }

    @Bean("slowThreadPool")
    public ThreadPoolTaskExecutor slowThreadPool() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(Constant.SLOW_THREAD_POOL_CORE_SIZE);// 设置核心线程数
        threadPoolTaskExecutor.setMaxPoolSize(Constant.SLOW_THREAD_POOL_MAX_SIZE);// 配置最大线程数
        threadPoolTaskExecutor.setQueueCapacity(Constant.SLOW_THREAD_POOL_QUEUE_SIZE);// 配置队列容量
        threadPoolTaskExecutor.setThreadNamePrefix(Constant.SLOW_THREAD_POOL_NAME);// 给线程池设置名称
        threadPoolTaskExecutor.setKeepAliveSeconds(Constant.SLOW_THREAD_POOL_KEEP_LIVE);
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());// 设置任务的拒绝策略
        return threadPoolTaskExecutor;
    }

    @Bean("fightThreadPool")
    public ThreadPoolTaskExecutor fightThreadPool() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(Constant.SLOW_THREAD_POOL_CORE_SIZE);// 设置核心线程数
        threadPoolTaskExecutor.setMaxPoolSize(Constant.SLOW_THREAD_POOL_MAX_SIZE);// 配置最大线程数
        threadPoolTaskExecutor.setQueueCapacity(Constant.SLOW_THREAD_POOL_QUEUE_SIZE);// 配置队列容量
        threadPoolTaskExecutor.setThreadNamePrefix(Constant.SLOW_THREAD_POOL_NAME);// 给线程池设置名称
        threadPoolTaskExecutor.setKeepAliveSeconds(Constant.SLOW_THREAD_POOL_KEEP_LIVE);
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());// 设置任务的拒绝策略
        return threadPoolTaskExecutor;
    }
}
