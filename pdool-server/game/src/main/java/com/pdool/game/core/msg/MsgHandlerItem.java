package com.pdool.game.core.msg;

import com.google.protobuf.Parser;

import java.lang.reflect.Method;

/**
 * 消息对应的处理器
 */
public class MsgHandlerItem {
    private int msgId;
    Parser in;
    Method newBuilder;
    IHandler handler;

    public MsgHandlerItem(int msgId, Parser in, Method newBuilder, IHandler handler) {
        this.msgId = msgId;
        this.in = in;
        this.newBuilder = newBuilder;
        this.handler = handler;
    }

    public int getMsgId() {
        return msgId;
    }

    public void setMsgId(int msgId) {
        this.msgId = msgId;
    }

    public Parser getIn() {
        return in;
    }

    public void setIn(Parser in) {
        this.in = in;
    }

    public Method getNewBuilder() {
        return newBuilder;
    }

    public void setNewBuilder(Method newBuilder) {
        this.newBuilder = newBuilder;
    }

    public IHandler getHandler() {
        return handler;
    }

    public void setHandler(IHandler handler) {
        this.handler = handler;
    }
}
