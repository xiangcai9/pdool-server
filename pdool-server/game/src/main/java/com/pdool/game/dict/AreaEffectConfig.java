package com.pdool.game.dict;


import com.pdool.common.dict.AreaEffectVo;
import com.pdool.game.core.cache.AbsConfigCache;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * 区域效果
 */
@Component
public class AreaEffectConfig extends AbsConfigCache<Integer, AreaEffectVo> {

    public AreaEffectConfig() {
        super("区域效果表_area_effect.xlsx");
    }

    @Override
    protected AreaEffectVo convert2Value(Map<Integer, String> rowData) {
        int id = Integer.parseInt(rowData.get(1));
        AreaEffectVo.AreaEffectVoBuilder builder = AreaEffectVo.builder();
        if (StringUtils.hasText(rowData.get(2)) ){
            int conditionId = Integer.parseInt(rowData.get(2));
            builder.effectId(conditionId);
        }
        AreaEffectVo build = builder.id(id).build();
        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        return Integer.valueOf(rowData.get(1));
    }
}
