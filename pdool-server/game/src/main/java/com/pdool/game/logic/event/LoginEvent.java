package com.pdool.game.logic.event;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 登录事件
 */
@Getter
public class LoginEvent extends ApplicationEvent {
    private final long roleId;
    public LoginEvent(Object source,long roleId) {
        super(source);
        this.roleId = roleId;
    }
}
