package com.pdool.game.logic.enums;

/**
 * 数值类型枚举
 */
public enum NumTypeEnum {
    //  对局次数
    BATTLE_COUNT(1),
    //  对局胜利次数
    BATTLE_WIN_COUNT(2),
    //  卡牌树经验
    CARD_TREE_EXP(3),
    //  积分
    SCORE(4),
    ;

    int numType;

    public int getNumType() {
        return numType;
    }

    NumTypeEnum(int numType) {
        this.numType = numType;
    }
}
