package com.pdool.game.util;

import cn.hutool.core.lang.WeightRandom;
import cn.hutool.core.util.RandomUtil;

/**
 * 掉落工具类
 */
public class DropUtil {


    public static String calcDrop(Iterable<WeightRandom.WeightObj<String>> weightObjs) {
        WeightRandom<String> stringWeightRandom = RandomUtil.weightRandom(weightObjs);
        String next = stringWeightRandom.next();
        return next;
    }
}
