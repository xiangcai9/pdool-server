package com.pdool.game.cache;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdool.common.entity.CardGroupEntity;
import com.pdool.common.mapper.impl.CardGroupDaoImpl;
import com.pdool.game.logic.domain.entity.vo.CardGroupVo;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 卡组缓存
 */
@Component
public class CardGroupCache {
    @Resource
    CardGroupDaoImpl cardGroupRepository;

    @Cacheable(value = "cardGroup", key = "#roleId")
    public Map<Integer, CardGroupVo> getAllCardGroup(Long roleId) {
        QueryWrapper<CardGroupEntity> wrapper=new QueryWrapper<>();
        wrapper.eq("role_id",roleId); //name不为空的用户
        List<CardGroupEntity> allByRoleId = cardGroupRepository.list(wrapper);
        Map<Integer, CardGroupVo> result = new HashMap<>();
        for (CardGroupEntity cardGroupEntity : allByRoleId) {
            Integer groupIndex = cardGroupEntity.getGroupIndex();
            CardGroupVo cardGroupVo = new CardGroupVo(cardGroupEntity);
            result.put(groupIndex, cardGroupVo);
        }
        return result;
    }

    @Transactional
    @CachePut(value = "cardGroup", key = "#group.entity.roleId")
    public Map<Integer, CardGroupVo>  updateCardGroup(Map<Integer, CardGroupVo> allCardGroup, CardGroupVo group) {
        cardGroupRepository.saveOrUpdate(group.toEntity());
        allCardGroup.put(group.getEntity().getGroupIndex(), group);
        return allCardGroup;
    }


}
