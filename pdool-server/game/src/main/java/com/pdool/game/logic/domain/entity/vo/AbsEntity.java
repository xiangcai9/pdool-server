package com.pdool.game.logic.domain.entity.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public abstract class AbsEntity<T> {
    T entity;

    public AbsEntity(T entity) {
        this.entity = entity;
    }

    public T toEntity() {
        return entity;
    }

    public T getEntity() {
        return entity;
    }
}
