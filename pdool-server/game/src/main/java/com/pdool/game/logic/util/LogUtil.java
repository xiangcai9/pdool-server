package com.pdool.game.logic.util;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdool.common.entity.StatPointEntity;
import com.pdool.common.mapper.impl.StatPointDaoImpl;
import com.pdool.game.logic.enums.LogPointType;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * 日志工具类
 */
@Component
@Slf4j
public class LogUtil {
    static LogUtil instance;

    public LogUtil() {
        instance = this;
    }

    public static LogUtil getInstance() {
        return instance;
    }

    @Resource
    StatPointDaoImpl statisticDao;


    public void recordLog(long roleId, LogPointType logPointType, long count) {
        try {
            QueryWrapper<StatPointEntity> wrapper = new QueryWrapper<>();
            wrapper.eq("role_id", roleId);
            String today = DateUtil.today();
            wrapper.eq("stat_date", today);
            wrapper.eq("type", logPointType.getType());

            StatPointEntity one = statisticDao.getOne(wrapper);
            if (one == null) {
                one = new StatPointEntity();
                one.setRoleId(roleId);
                one.setStatDate(today);
                one.setValue(0L);
            }
            one.setType(logPointType.getType());
            one.setValue(one.getValue() + count);
            one.setSaveTime(new Date());
            statisticDao.saveOrUpdate(one);
        } catch (Exception e) {
            log.error("", e);
        }
    }


}
