package com.pdool.game.core.res.impl;

import com.pdool.common.dict.CardInfoConfigVo;
import com.pdool.common.entity.CardInfoEntity;
import com.pdool.game.core.res.AbsRes;
import com.pdool.game.dict.CardConfig;
import com.pdool.game.logic.service.CardService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 卡牌
 * 配置：4_cardId
 *
 * @author 香菜
 */
@Component("4")
public class CardRes extends AbsRes<Integer> {
    @Resource
    CardService cardService;
    @Resource
    CardConfig cardConfig;

    @Override
    public boolean canAdd(long roleId, Integer add) {

        return true;
    }

    @Override
    public boolean canDec(long roleId, Integer cost) {
        return false;
    }

    @Override
    public boolean add(long roleId, Integer add) {
        CardInfoConfigVo cardInfoConfigVo = cardConfig.getDataMap().get(add);
        CardInfoEntity cardInfoEntity = new CardInfoEntity();
        cardInfoEntity.setRoleId(roleId);
        cardInfoEntity.setLv(cardInfoConfigVo.getLv());
        cardInfoEntity.setCardId(cardInfoConfigVo.getId());
        cardService.updateCard(cardService.getAllCard(roleId),cardInfoEntity);
        return true;
    }

    @Override
    public boolean dec(long roleId, Integer add) {
        return false;
    }

    @Override
    public Integer merge(long roleId, String[] add, Integer old) {
        if (old == null) {
            old = 0;
        }
        int addNum = Integer.parseInt(add[1]);
        return addNum + old;
    }
}
