package com.pdool.game.cache;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdool.common.entity.CardInfoEntity;
import com.pdool.common.entity.CardPieceEntity;
import com.pdool.common.mapper.impl.CardInfoDaoImpl;
import com.pdool.common.mapper.impl.CardPieceDaoImpl;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 卡牌缓存
 */
@Component
public class CardCache {
    @Resource
    CardInfoDaoImpl cardInfoRepository;
    @Resource
    CardPieceDaoImpl cardPieceDaoImpl;

    @Cacheable(value = "cardInfo", key = "#roleId")
    public Map<Integer, CardInfoEntity> getAllCard(Long roleId) {
        QueryWrapper<CardInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId); //name不为空的用户
        List<CardInfoEntity> allByRoleId = cardInfoRepository.list(wrapper);
        Map<Integer, CardInfoEntity> collect = allByRoleId.stream().collect(Collectors.toMap(CardInfoEntity::getCardId, e -> e));
        return collect;
    }

    @Transactional
    @CachePut(value = "cardInfo", key = "#cardInfoEntity.roleId")
    public Map<Integer, CardInfoEntity> updateCard(Map<Integer, CardInfoEntity> allCard,CardInfoEntity cardInfoEntity) {
        cardInfoRepository.saveOrUpdate(cardInfoEntity);
        allCard.put(cardInfoEntity.getCardId(), cardInfoEntity);
        return allCard;
    }


    @Cacheable(value = "cardPiece", key = "#roleId")
    public Map<Integer, CardPieceEntity> getAllPiece(Long roleId) {
        QueryWrapper<CardPieceEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId); //name不为空的用户
        List<CardPieceEntity> allByRoleId = cardPieceDaoImpl.list(wrapper);
        Map<Integer, CardPieceEntity> collect = allByRoleId.stream().collect(Collectors.toMap(CardPieceEntity::getPieceId, e -> e));
        return collect;
    }

    @Transactional
    @CachePut(value = "cardPiece", key = "#cardPieceEntity.roleId")
    public Map<Integer, CardPieceEntity>  updateCardPiece(Map<Integer, CardPieceEntity> allPiece,CardPieceEntity cardPieceEntity) {
        cardPieceDaoImpl.saveOrUpdate(cardPieceEntity);
        allPiece.put(cardPieceEntity.getPieceId(),cardPieceEntity);
        return allPiece;
    }


}
