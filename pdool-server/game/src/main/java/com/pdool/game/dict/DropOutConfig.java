package com.pdool.game.dict;

import cn.hutool.core.lang.WeightRandom;
import com.pdool.common.dict.DropOutVo;
import com.pdool.game.core.cache.AbsConfigCache;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 掉落配置
 */
@Component
public class DropOutConfig extends AbsConfigCache<String, DropOutVo> {

    public DropOutConfig() {
        super("掉落表_drop_out.xlsx",3);
    }


    @Override
    protected DropOutVo convert2Value(Map<Integer, String> rowData) {
        String tagId = rowData.get(3);
        DropOutVo orDefault = getDataMap().getOrDefault(tagId, new DropOutVo());
        orDefault.setTagId(tagId);
        double s = Double.parseDouble(rowData.get(5));
        String content = rowData.get(6);
        WeightRandom.WeightObj<String> stringWeightObj = new WeightRandom.WeightObj<>(content, s);
        orDefault.getWeightObjs().add(stringWeightObj);

        return orDefault;
    }

    @Override
    protected String convert2Key(Map<Integer, String> rowData) {
        String s = rowData.get(3);
        return s;
    }
}
