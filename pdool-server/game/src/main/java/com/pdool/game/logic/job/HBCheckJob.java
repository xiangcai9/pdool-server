package com.pdool.game.logic.job;

import com.pdool.common.constants.Constant;
import com.pdool.game.logic.mgr.UserMgr;
import com.pdool.game.logic.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 整点执行，检查心跳，清理没用的连接
 */
@Component
@Slf4j
public class HBCheckJob {
    @Resource
    LoginService loginService;

    @Scheduled(fixedRate = 1000)
    public void doJob() {
        long l = System.currentTimeMillis();
        for (Map.Entry<Long, Long> entry : UserMgr.getInstance().getHBMap().entrySet()) {
            long roleId = entry.getKey();
            Long time = entry.getValue();
            if (l - time >= Constant.TIME_OUT_LOGOUT_MIN * 60 * 1000) {
                WebSocketSession webSocketSession = UserMgr.getInstance().getSessionMap().get(roleId);
                loginService.logOut(webSocketSession);
            }
        }
    }
}
