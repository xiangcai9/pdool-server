package com.pdool.game.dict;

import cn.hutool.core.lang.Pair;
import com.pdool.common.constants.Constant;
import com.pdool.common.dict.AICardVo;
import com.pdool.game.core.cache.AbsConfigCache;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 加载AI卡牌数据
 */
@Component
public class AICardConfig extends AbsConfigCache<Integer, AICardVo> {

    public AICardConfig() {
        super("AI_aicard.xlsx",3);
    }

    @Override
    protected AICardVo convert2Value(Map<Integer, String> rowData) {
        int id = Integer.valueOf(rowData.get(1));
        String[] split = rowData.get(2).split(Constant.SPLIT_COMMA);
        List<Integer> cardList = Arrays.stream(split).map(Integer::parseInt).collect(Collectors.toList());
        String s = rowData.get(3);
        String[] split1 = s.split(Constant.SPLIT_COMMA);

        int low = Integer.parseInt(split1[0]);
        int high = Integer.parseInt(split1[1]);

        AICardVo build = AICardVo.builder().cardList(cardList).id(id).battleNum(new Pair<>(low, high)).build();

        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        return Integer.valueOf(rowData.get(1));
    }
}
