package com.pdool.game.logic.service;

import com.pdool.common.constants.Constant;
import com.pdool.common.constants.ErrorCode;
import com.pdool.common.entity.RoleNumInfoEntity;
import com.pdool.common.exception.ErrorException;
import com.pdool.common.msg.match.MatchReq;
import com.pdool.game.cache.RoleCache;
import com.pdool.game.dict.BasePropConfig;
import com.pdool.game.logic.domain.MatchRoleInfo;
import com.pdool.game.logic.domain.entity.vo.CardGroupVo;
import com.pdool.game.logic.enums.NumTypeEnum;
import com.pdool.game.logic.mgr.MatchMgr;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;
/**
 * 匹配逻辑处理类
 */
@Component
public class MatchService {
    @Resource
    RoleCache roleCache;
    @Resource
    CardGroupService cardGroupService;
    @Resource
    BasePropConfig propConfig;
    public void matchReq(long roleId,MatchReq req){
        CardGroupVo cardGroup = cardGroupService.getCardGroup(roleId, req.getCardGroupIndex());

        int  needNum = Integer.parseInt(propConfig.getByKey(Constant.PROP_FIGHT_GROUP_NUM).getValue());
        if (cardGroup.getCardGroupList().size() != needNum){
            throw new ErrorException(ErrorCode.FIGHT_CARD_GROUP_NOT_ENOUGH);
        }

        Map<Integer, RoleNumInfoEntity> roleNumInfo = roleCache.queryRoleNumInfo(roleId);
        long sumCount = roleNumInfo.getOrDefault(NumTypeEnum.BATTLE_COUNT.getNumType(),new RoleNumInfoEntity()).getNum();
        MatchRoleInfo matchRoleInfo = new MatchRoleInfo();
        matchRoleInfo.setMatchGroup((int) (sumCount/ Constant.MATCH_GROUP));
        matchRoleInfo.setCardGroupIndex(req.getCardGroupIndex());
        matchRoleInfo.setRoleId(roleId);
        matchRoleInfo.setRegTime(new Date());




        MatchMgr.getInstance().matchReq(matchRoleInfo);
    }
}
