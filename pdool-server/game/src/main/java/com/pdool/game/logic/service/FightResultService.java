package com.pdool.game.logic.service;

import cn.hutool.core.util.RandomUtil;
import com.pdool.common.constants.Constant;
import com.pdool.common.constants.MsgId;
import com.pdool.common.dict.BasePropVo;
import com.pdool.common.dict.DropOutVo;
import com.pdool.common.dto.FightResultDto;
import com.pdool.common.msg.fight.FightResultSTC;
import com.pdool.game.core.res.ResOp;
import com.pdool.game.dict.BasePropConfig;
import com.pdool.game.dict.DropOutConfig;
import com.pdool.game.logic.enums.NumTypeEnum;
import com.pdool.game.logic.mgr.MatchMgr;
import com.pdool.game.util.DropUtil;
import com.pdool.game.util.SessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * 战斗结束处理类
 */
@Service
@Slf4j
public class FightResultService {
    @Resource
    BasePropConfig basePropConfig;
    @Resource
    DropOutConfig dropOutConfig;
    @Resource
    RoleService roleService;

    public void onFightEnd(FightResultDto dto) {

        String value = basePropConfig.getByKey(Constant.PROP_FIGHT_DROP).getValue();
        String scoreConfig = basePropConfig.getByKey(Constant.PROP_FIGHT_SCORE).getValue();
        String[] scoreArr = scoreConfig.split(Constant.SPLIT_COMMA);
        String[] split = value.split(Constant.SPLIT_COMMA);
        String winDropId = split[0];
        String loseDropId = split[1];
        String tieDropId = split[2];
        //  1、发送奖励 TODO
        List<Long> roleList = dto.getRoleList();

        BiConsumer<Long, Integer> consumer = (roleId, result) -> {
            String dropId;
            String score;
            if (result == 3) {
                dropId = tieDropId;
                score = scoreArr[1];
            } else if (result == 2) {//失败
                dropId = loseDropId;
                score = scoreArr[2];

            } else {//   胜利
                dropId = winDropId;
                score = scoreArr[0];
            }
            if (roleId < 0) {
                return;
            }
            FightResultSTC.Builder builder = FightResultSTC.newBuilder();

            DropOutVo byKey = dropOutConfig.getByKey(dropId);
            String dropReward = DropUtil.calcDrop(byKey.getWeightObjs());

            builder.setResult(result);
            builder.setReward(dropReward);
            ResOp resOp = new ResOp(roleId);
            resOp.add(dropReward);
            resOp.add(score);

            resOp.op();
            SessionUtil.pushMsg(roleId, MsgId.PUSH_FIGHT_RESULT, builder.build());
            clearRoomAttr(roleId);

        };

        //  平局
        if (dto.getLoserSet().size() == 0 && dto.getWinnerSet().size() == 0) {
            dto.getRoleList().forEach(tieRoleId -> consumer.accept(tieRoleId,3));
        } else {
            //  赢者组
            for (Long winnerId : dto.getWinnerSet()) {
                //  2、更新获胜次数
                roleService.updateRoleNum(winnerId, NumTypeEnum.BATTLE_WIN_COUNT, 1);

                consumer.accept(winnerId,1);
            }
            //  败者组
            for (Long loseId : dto.getLoserSet()) {
                consumer.accept(loseId,2);
            }
        }

        log.error("roomId {}  roleList  {}  winnerId  {}      loseId {}  ", dto.getRoomId(), roleList, dto.getWinnerSet(),dto.getLoserSet());
    }

    /**
     * 清除房间属性
     *
     * @param roleId
     */
    private void clearRoomAttr(Long roleId) {
        MatchMgr.getInstance().getRoleRoomMap().remove(roleId);
    }


    public int getPieceDropCount() {
        BasePropVo byKey = basePropConfig.getByKey(Constant.PROP_FIGHT_DROP_NUM);
        String value = byKey.getValue();
        String[] split = value.split(Constant.SPLIT_COMMA);
        int dropMin = Integer.parseInt(split[0]);
        int dropMax = Integer.parseInt(split[1]);
        int i = RandomUtil.randomInt(dropMin, dropMax + 1);
        return i;
    }
}
