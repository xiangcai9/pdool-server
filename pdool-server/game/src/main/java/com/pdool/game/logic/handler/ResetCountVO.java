package com.pdool.game.logic.handler;
import cn.hutool.core.date.DateUtil;
import com.pdool.game.logic.enums.ResetLimitType;

import java.util.Date;

/**
 * 重置次数
 */
public class ResetCountVO {
    // 角色id
    private long roleId;
    // 重置的类型
    private ResetLimitType resetType;
    //已经消耗次数
    private int num;
    //最后更新时间
    private Date updateTime;
    //扩展信息
    private String extend;

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public ResetLimitType getResetType() {
        return resetType;
    }

    public void setResetType(ResetLimitType resetType) {
        this.resetType = resetType;
    }

    /**
     * 获得次数
     *
     * @return
     */
    public int getCount() {
        int resetType = this.resetType.getResetType();
        Date now = new Date();
        if (resetType == 1) {
            // 每日重置
            if (DateUtil.isSameDay(now, updateTime)) {
                return this.num;
            }
        } else if (resetType == 2) {
            // 每周重置
            if (DateUtil.isSameWeek(now, updateTime,false)) {
                return this.num;
            }
        } else if (resetType == 3) {
            // 每月重置
            if (DateUtil.isSameMonth(now, updateTime)) {
                return this.num;
            }
        }
        this.num = 0;
        updateTime = now;
        return 0;
    }

    /**
     * 增加次数
     *
     * @param addCountArg
     * @return
     */
    public int addCount(int... addCountArg) {
        int count = getCount();
        int addCount = 1;
        if (addCountArg.length != 0) {
            addCount = addCountArg[0];
        }
        count += addCount;
        num = count;
        updateTime = new Date();
        return count;
    }


}