package com.pdool.game.logic.service;

import com.pdool.common.constants.MsgId;
import com.pdool.common.entity.CardInfoEntity;
import com.pdool.common.entity.CardPieceEntity;
import com.pdool.common.msg.card.CardInfoSTC;
import com.pdool.common.msg.card.CardPieceResp;
import com.pdool.game.cache.CardCache;
import com.pdool.game.util.SessionUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 卡牌服务类
 */
@Service
public class CardService {
    @Resource
    CardCache cardCache;


    public Map<Integer, CardInfoEntity> getAllCard(long roleId){
        Map<Integer, CardInfoEntity> allCard = cardCache.getAllCard(roleId);
        return allCard;
    }

    public void updateCard(Map<Integer, CardInfoEntity> allCard, CardInfoEntity cardInfoEntity){
        cardCache.updateCard(allCard,cardInfoEntity);
    }

    public Map<Integer, CardPieceEntity> getAllCardPiece(Long roleId){
        Map<Integer, CardPieceEntity> allCard = cardCache.getAllPiece(roleId);
        return allCard;
    }
    public void updateCardPiece(Map<Integer, CardPieceEntity> allCardPiece, CardPieceEntity cardPieceEntity){
        cardCache.updateCardPiece(allCardPiece,cardPieceEntity);
    }




    public void pushCardInfo(long roleId) {
        CardInfoSTC.Builder builder = CardInfoSTC.newBuilder();
        Map<Integer, CardInfoEntity> allCard = getAllCard(roleId);
        for (CardInfoEntity value : allCard.values()) {
            CardInfoSTC.CardInfo.Builder cardInfo = CardInfoSTC.CardInfo.newBuilder();
            cardInfo.setId(value.getId());
            cardInfo.setCardId(value.getCardId());
            cardInfo.setLv(value.getLv());
            builder.addCardInfoList(cardInfo);
        }
        SessionUtil.pushMsg(roleId, MsgId.PUSH_CARD_INFO,builder.build());
    }


    public void pushCardPiece(long roleId) {
        Map<Integer, CardPieceEntity> allCardPiece = getAllCardPiece(roleId);
        CardPieceResp.Builder builder = CardPieceResp.newBuilder();
        for (CardPieceEntity value : allCardPiece.values()) {
            CardPieceResp.PieceItem.Builder builder1 = CardPieceResp.PieceItem.newBuilder().setPieceId(value.getPieceId()).setNum(value.getNum());
            builder.addPieceList(builder1);
        }
        SessionUtil.pushMsg(roleId, MsgId.QUERY_CARD_PIECE,builder.build());
    }
}
