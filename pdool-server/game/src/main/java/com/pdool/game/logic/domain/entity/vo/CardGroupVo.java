package com.pdool.game.logic.domain.entity.vo;

import cn.hutool.core.text.StrJoiner;
import com.pdool.common.constants.Constant;
import com.pdool.common.entity.CardGroupEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Getter
@NoArgsConstructor
public class CardGroupVo extends AbsEntity<CardGroupEntity> {

    private List<Integer> cardGroupList = new ArrayList<>();


    public CardGroupVo(CardGroupEntity entity) {
        super(entity);
        if (StringUtils.hasText(entity.getCardGroup())){
            String[] split = entity.getCardGroup().split(Constant.SPLIT_COMMA);
            for (String s : split) {
                cardGroupList.add(Integer.valueOf(s));
            }
        }

    }

    @Override
    public CardGroupEntity toEntity() {
        entity.setCardGroup(StrJoiner.of(Constant.SPLIT_COMMA).append(cardGroupList).toString());
        return entity;
    }


}
