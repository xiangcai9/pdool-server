package com.pdool.game.cache;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdool.common.entity.GuideInfoEntity;
import com.pdool.common.mapper.impl.GuideInfoDaoImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 引导缓存，暂时没缓存
 */
@Slf4j
@Service
public class GuideInfoCache {
    @Resource
    GuideInfoDaoImpl guideInfoDao;

    public GuideInfoEntity queryRoleOne(Long roleId) {
        QueryWrapper<GuideInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id", roleId);
        GuideInfoEntity one = guideInfoDao.getOne(wrapper);
        if (one == null){
            one = new GuideInfoEntity();
            one.setRoleId(roleId);
        }
        return one;
    }

    public void updateUserGuide(GuideInfoEntity guideInfoEntity) {
        guideInfoDao.saveOrUpdate(guideInfoEntity);
    }

}
