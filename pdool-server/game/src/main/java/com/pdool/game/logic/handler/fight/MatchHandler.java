package com.pdool.game.logic.handler.fight;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.common.NullResp;
import com.pdool.common.msg.match.MatchReq;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.service.MatchService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 匹配处理器
 */
@MsgHandler(MsgId.MATCH)
@Component
public class MatchHandler implements IHandler<MatchReq, NullResp> {
    @Resource
    MatchService matchService;
    @Override
    public NullResp handle(Long roleId, MatchReq matchReq) {
        matchService.matchReq(roleId,matchReq);
        return null;
    }
}
