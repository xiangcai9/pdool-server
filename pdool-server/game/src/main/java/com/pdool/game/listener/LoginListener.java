package com.pdool.game.listener;

import com.pdool.common.entity.RoleInfoEntity;
import com.pdool.game.logic.event.LoginEvent;
import com.pdool.game.logic.mgr.UserMgr;
import com.pdool.game.logic.service.CardService;
import com.pdool.game.logic.service.GuideService;
import com.pdool.game.logic.service.RoleService;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 登录listener
 */
@Component
public class LoginListener implements ApplicationListener<LoginEvent> {
    @Resource
    RoleService roleService;
    @Resource
    CardService cardService;

    @Resource
    GuideService guideService;
    @Override
    public void onApplicationEvent(LoginEvent event) {
        long roleId = event.getRoleId();
        RoleInfoEntity roleInfoEntity = roleService.queryUserOne(roleId);
        UserMgr.getInstance().getRoleNameMap().put(roleId,roleInfoEntity.getName());
        guideService.pushGuideInfo(roleId);
        roleService.pushRoleInfo(roleId);
        roleService.pushNumInfo(roleId);
        cardService.pushCardInfo(roleId);

    }

}
