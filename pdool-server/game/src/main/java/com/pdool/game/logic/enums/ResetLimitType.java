package com.pdool.game.logic.enums;
/**
 * 重置次数限制类型
 */
public enum ResetLimitType {
    /**
     * 每日抽奖次数
     */
    LOTTERY_COUNT(1),
    /**
     * 每周收礼次数
     */
    RECEIVE_GIFT_COUNT(2,2),
    ;
    /**
     * 限制类型
     */
    private int limitType;
    /**
     * 重置类型 1 每天 2 每周 3 每月
     */
    private int resetType = 1;

    ResetLimitType(int limitType) {
        this.limitType = limitType;
    }

    ResetLimitType(int limitType, int resetType) {
        this.limitType = limitType;
        this.resetType = resetType;
    }

    public int getLimitType() {
        return limitType;
    }

    public int getResetType() {
        return resetType;
    }

    public static ResetLimitType valueOf(int limitType) {
        for (ResetLimitType resetLimitType : values()) {
            if (limitType == resetLimitType.getLimitType()) {
                return resetLimitType;
            }
        }
        return null;
    }
}