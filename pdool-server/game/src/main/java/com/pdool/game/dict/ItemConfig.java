package com.pdool.game.dict;

import com.pdool.common.dict.PropVo;
import com.pdool.game.core.cache.AbsConfigCache;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 道具表配置
 */
@Component
public class ItemConfig extends AbsConfigCache<Integer, PropVo> {
    public ItemConfig() {
        super("道具表_prop.xlsx",3);
    }

    @Override
    protected PropVo convert2Value(Map<Integer, String> rowData) {
        int id = Integer.parseInt(rowData.get(1));
        String name = rowData.get(2);
        int itemType = Integer.parseInt(rowData.get(3));
        int quality = Integer.parseInt(rowData.get(4));
        int bag = Integer.parseInt(rowData.get(5));
        String price = rowData.get(6);

        PropVo build = PropVo.builder()
                .itemType(itemType)
                .bag(bag)
                .id(id)
                .quality(quality)
                .name(name)
                .price(price).build();

        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        String s = rowData.get(1);
        return Integer.valueOf(s);
    }
}
