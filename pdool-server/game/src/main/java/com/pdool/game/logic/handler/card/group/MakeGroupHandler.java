package com.pdool.game.logic.handler.card.group;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.card.MakeGroupReq;
import com.pdool.common.msg.common.NullResp;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.service.CardGroupService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 创建卡组
 */
@MsgHandler(MsgId.CARD_MAKE_GROUP)
@Component
public class MakeGroupHandler implements IHandler<MakeGroupReq, NullResp> {
    @Resource
    CardGroupService cardGroupService;

    @Override
    public NullResp handle(Long roleId, MakeGroupReq makeGroupReq) {

        cardGroupService.makeGroup(roleId,makeGroupReq);
        return null;
    }
}
