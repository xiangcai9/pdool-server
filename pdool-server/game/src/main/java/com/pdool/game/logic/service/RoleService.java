package com.pdool.game.logic.service;

import com.pdool.common.constants.MsgId;
import com.pdool.common.entity.RoleInfoEntity;
import com.pdool.common.entity.RoleNumInfoEntity;
import com.pdool.common.mapper.impl.RoleInfoDaoImpl;
import com.pdool.common.msg.login.NumInfoItem;
import com.pdool.common.msg.login.RoleInfoSTC;
import com.pdool.common.msg.login.RoleNumInfoSTC;
import com.pdool.game.cache.RoleCache;
import com.pdool.game.logic.enums.NumTypeEnum;
import com.pdool.game.util.SessionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 角色逻辑处理类
 */
@Service
@Slf4j
public class RoleService {
    @Resource
    RoleCache userCache;
    @Resource
    RoleInfoDaoImpl playerRepository;
    public RoleInfoEntity queryUserOne(long roleId) {
        return userCache.queryRoleOne(roleId);
    }

    public void updateUser(RoleInfoEntity roleInfoEntity) {
        userCache.updateUser(roleInfoEntity);
    }

    public List<Map<String,Object>> queryAllRoleNames(){
        return  playerRepository.getBaseMapper().findAllRoleName();
    }

    public Long updateRoleNum(long roleId, NumTypeEnum typeEnum, long exp) {
        Map<Integer, RoleNumInfoEntity> numMap = userCache.queryRoleNumInfo(roleId);
        userCache.updateRoleNum(roleId, numMap, typeEnum, exp, null);
        return numMap.get(typeEnum.getNumType()).getNum();
    }

    public void updateRoleNum(long roleId, NumTypeEnum typeEnum, long exp, String extend) {
        Map<Integer, RoleNumInfoEntity> numMap = userCache.queryRoleNumInfo(roleId);
        userCache.updateRoleNum(roleId, numMap, typeEnum, exp, extend);
    }

    public void updateRoleNumExtend(long roleId, NumTypeEnum typeEnum, String extend) {
        Map<Integer, RoleNumInfoEntity> numMap = userCache.queryRoleNumInfo(roleId);
        userCache.updateRoleNum(roleId, numMap, typeEnum, null, extend);
    }

    public RoleNumInfoEntity getRoleNumInfo(long roleId, NumTypeEnum numTypeEnum) {
        Map<Integer, RoleNumInfoEntity> numMap = userCache.queryRoleNumInfo(roleId);
        RoleNumInfoEntity orDefault = numMap.getOrDefault(numTypeEnum.getNumType(), new RoleNumInfoEntity());
        return orDefault;
    }

    public void pushRoleInfo(long roleId) {
        RoleInfoEntity roleInfoEntity = queryUserOne(roleId);

        RoleInfoSTC.Builder builder = RoleInfoSTC.newBuilder();
        builder.setRoleId(roleInfoEntity.getRoleId());
        builder.setRoleType(roleInfoEntity.getRoleType());
        builder.setName(roleInfoEntity.getName());

        SessionUtil.pushMsg(roleId, MsgId.PUSH_ROLE_INFO, builder.build());
    }

    public void pushNumInfo(long roleId) {

        Map<Integer, RoleNumInfoEntity> numInfoEntityMap = userCache.queryRoleNumInfo(roleId);
        RoleNumInfoSTC.Builder builder = RoleNumInfoSTC.newBuilder();

        for (RoleNumInfoEntity value : numInfoEntityMap.values()) {
            NumInfoItem.Builder builder1 = NumInfoItem.newBuilder();
            builder1.setNum(value.getNum());
            builder1.setNumType(value.getNumType());
            if (StringUtils.hasText(value.getExtend())) {
                builder1.setExtend(value.getExtend());
            }
            builder.addItemList(builder1);
        }
        SessionUtil.pushMsg(roleId, MsgId.PUSH_ROLE_NUM_INFO, builder.build());
    }
    
    
    public void pushSingleNumChange(long roleId, NumTypeEnum numType){
        try {
            Map<Integer, RoleNumInfoEntity> numInfoEntityMap = userCache.queryRoleNumInfo(roleId);
            NumInfoItem.Builder builder1 = NumInfoItem.newBuilder();
            RoleNumInfoEntity roleNumInfoEntity = numInfoEntityMap.get(numType.getNumType());

            builder1.setNumType(numType.getNumType());
            builder1.setNum(roleNumInfoEntity.getNum());
//            builder1.setExtend(roleNumInfoEntity.getExtend());

            SessionUtil.pushMsg(roleId, MsgId.PUSH_SINGLE_NUM_TYPE, builder1.build());
        } catch (Exception e) {
            log.error("",e);
        }
    }
}
