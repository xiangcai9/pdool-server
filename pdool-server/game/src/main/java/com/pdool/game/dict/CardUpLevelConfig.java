package com.pdool.game.dict;

import com.pdool.common.dict.CardUpLevelVo;
import com.pdool.game.core.cache.AbsConfigCache;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 卡牌升级表
 */
@Component
public class CardUpLevelConfig extends AbsConfigCache<Integer, CardUpLevelVo> {
    public CardUpLevelConfig() {
        super("卡牌升级表_card_uplevel.xlsx", 3);
    }

    @Override
    protected CardUpLevelVo convert2Value(Map<Integer, String> rowData) {
        Integer s = Integer.valueOf(rowData.get(1));
        String cost = rowData.get(2);
        String add = rowData.get(3);
        CardUpLevelVo build = CardUpLevelVo.builder().id(s).cost(cost).cardTreeAdd(add).build();
        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        return Integer.valueOf(rowData.get(1));
    }
}
