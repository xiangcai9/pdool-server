package com.pdool.game.logic.enums;
/**
 * 重置次数限制类型
 */
public enum LogPointType {
    /**
     * 24小时内用户开启牌局的次数
     */
    BATTLE_COUNT(1),
    /**
     * .需要统计用户驻留在游戏中的总时长和每天驻留的时长
     */
    DAY_IN_GAME(2),
    /**
     * 需要统计24小时内同一用户的登录次数
     */
    LOGIN_COUNT(3),
    ;
    private int type;


    LogPointType(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}