package com.pdool.game.core.controller;

import com.pdool.common.msg.card.MakeGroupReq;
import com.pdool.common.msg.common.NullReq;
import com.pdool.common.msg.guide.SetGuideInfoReq;
import com.pdool.common.msg.rank.ScoreRankResp;
import com.pdool.game.core.msg.RoomRemoteMgr;
import com.pdool.game.core.res.ResOp;
import com.pdool.game.core.res.ResTypeManager;
import com.pdool.game.logic.domain.entity.vo.CardGroupVo;
import com.pdool.game.logic.handler.guide.SetGuideInfoHandler6004;
import com.pdool.game.logic.handler.rank.ScoreRankHandler;
import com.pdool.game.logic.service.CardGroupService;
import com.pdool.game.logic.service.RankService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.CacheManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 测试接口
 */
@RestController
@Slf4j
@RequestMapping("/test")
public class TestController {
    @Resource
    SetGuideInfoHandler6004 setGuideInfoHandler6004;
    @Resource
    ScoreRankHandler scoreRankHandler;

    @Resource
    RankService rankService;
    @Resource
    JdbcTemplate jdbcTemplate;

    @GetMapping("/test8/{roleId}")
    public String test8(@PathVariable Long roleId) throws IOException {
        List<Map<String, Object>> maps = jdbcTemplate.queryForList("select num,role_id from role_num_info where num_type = 4 ORDER BY num DESC LIMIT 100");
        for (Map<String, Object> map : maps) {
            long ss = Long.parseLong(map.get("role_id").toString());
            int s = Integer.parseInt(map.get("num").toString());
            rankService.updateScore(ss, s);
        }





        ScoreRankResp handle = scoreRankHandler.handle(1822206619L, NullReq.newBuilder().build());
        System.out.println();
        return "test8 result {}; ;";
    }

    @GetMapping("/test7/{stepId}")
    public String test7(@PathVariable Integer stepId) {
        long roleId = 15L;
        SetGuideInfoReq.Builder builder = SetGuideInfoReq.newBuilder();
        builder.setGuideId(1);
        builder.setStepId(stepId);
        setGuideInfoHandler6004.handle(roleId, builder.build());
        return "room";
    }

    @GetMapping("/test")
    public String test() {
        String room = RoomRemoteMgr.getInstance().getRoomService().createRoom(null);
        log.error("roomId   {}", room);
        return room;
    }

    @Resource
    CacheManager cacheManager;

    @Resource
    ResTypeManager resTypeManager;

    @Resource
    CardGroupService cardGroupService;

    @GetMapping("remote")
    public String test4() {
        String room = RoomRemoteMgr.getInstance().getRoomService().sayHello("helloWorld");
        return room;
    }

    @GetMapping("test3")
    public void test3() {
//        Map<Integer, CardVo> all = cardConfig.getAll();
//        List<ApiUserEntity> apiUserEntities = userService.queryUser();
        long roleId = 1L;
        ResOp resOp = new ResOp(roleId);
        resOp.add("2_10001_1000");
        resOp.op();
        System.out.println();
    }


    @GetMapping("test2/{lv}")
    public void test2(@PathVariable int lv) {
        MakeGroupReq.Builder builder = MakeGroupReq.newBuilder();
        builder.setGroupIndex(1);
        ArrayList arrayList = new ArrayList<>();
        arrayList.add(1);
        arrayList.add(2);
        cardGroupService.makeGroup(1L, builder.build());

    }

    @GetMapping("test5")
    public void test5() {

        CardGroupVo cardGroup = cardGroupService.getCardGroup(1, 1);


        System.out.println();
    }

}
