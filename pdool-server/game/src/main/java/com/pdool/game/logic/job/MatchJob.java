package com.pdool.game.logic.job;

import com.pdool.game.logic.mgr.MatchMgr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 匹配的job
 */
@Component
@Slf4j
public class MatchJob {
    @Resource
    MatchMgr matchMgr;

    @Scheduled(fixedRate = 1000)
    public void checkMatch(){
        matchMgr.checkMatch();
    }
}
