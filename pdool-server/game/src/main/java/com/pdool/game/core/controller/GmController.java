package com.pdool.game.core.controller;

import com.pdool.game.logic.handler.login.LoginOutHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * gm
 */
@RestController
@Slf4j
@RequestMapping("/gm")
public class GmController {
    @Resource
    LoginOutHandler loginOutHandler;

    @GetMapping("/kickout/{roleId}")
    public String kickOut(@PathVariable Long roleId) throws IOException {
        loginOutHandler.handle(roleId,null);
        return "test8 result {}; ;";
    }


}
