package com.pdool.game.logic.handler.fight;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.common.HBReq;
import com.pdool.common.msg.common.NullResp;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.mgr.UserMgr;
import org.springframework.stereotype.Component;

/**
 * 心跳信息
 */
@MsgHandler(MsgId.HB)
@Component
public class HbHandler implements IHandler<HBReq, NullResp> {


    @Override
    public NullResp handle(Long roleId, HBReq matchReq) {

        UserMgr.getInstance().getHBMap().put(roleId,System.currentTimeMillis());

        return NullResp.newBuilder().build();
    }
}
