package com.pdool.game.logic.domain;

import lombok.Data;

import java.util.Date;

/**
 * 匹配信息
 */
@Data
public class MatchRoleInfo {
    private long roleId;
    private int matchGroup;
    private int cardGroupIndex;
    private Date regTime;
}
