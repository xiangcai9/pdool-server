package com.pdool.game.logic.handler.login;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.common.NullResp;
import com.pdool.common.msg.login.LogoutReq;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.mgr.UserMgr;
import com.pdool.game.logic.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 登出
 */
@Component
@MsgHandler(MsgId.LOGIN_OUT)
@Slf4j
public class LoginOutHandler implements IHandler<LogoutReq, NullResp> {
    @Resource
    LoginService loginService;
    @Override
    public NullResp handle(Long roleId, LogoutReq logoutReq) throws IOException {
        WebSocketSession webSocketSession = UserMgr.getInstance().getSessionMap().get(roleId);
        loginService.logOut(webSocketSession);
        log.error("------{}  logout ",roleId);
        return null;
    }
}