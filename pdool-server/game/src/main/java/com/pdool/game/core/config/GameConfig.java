package com.pdool.game.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * 游戏配置
 */
@Configuration
@ConfigurationProperties(prefix = "game")
@Data
public class GameConfig {
    private String excelPath;
    private String serverId;
    private String adminPwd;

}
