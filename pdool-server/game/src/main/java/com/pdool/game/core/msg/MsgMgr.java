package com.pdool.game.core.msg;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 管理消息配置
 */
@Component
public class MsgMgr {

    private static Map<Integer, MsgHandlerItem> handlerMap = new ConcurrentHashMap<>();

    public static Map<Integer, MsgHandlerItem> getHandlerMap() {
        return handlerMap;
    }
}
