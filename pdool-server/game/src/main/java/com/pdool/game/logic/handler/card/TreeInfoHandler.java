package com.pdool.game.logic.handler.card;

import com.pdool.common.constants.MsgId;
import com.pdool.common.entity.RoleNumInfoEntity;
import com.pdool.common.msg.card.CardTreeInfoResp;
import com.pdool.common.msg.common.NullReq;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.enums.NumTypeEnum;
import com.pdool.game.logic.service.RoleService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 卡牌树信息
 */
@MsgHandler(MsgId.CARD_TREE_EXP_ADD)
@Component
public class TreeInfoHandler implements IHandler<NullReq, CardTreeInfoResp> {
    @Resource
    RoleService roleService;

    @Override
    public CardTreeInfoResp handle(Long roleId, NullReq nullReq) {
        RoleNumInfoEntity roleNumInfo = roleService.getRoleNumInfo(roleId, NumTypeEnum.CARD_TREE_EXP);
        CardTreeInfoResp.Builder builder = CardTreeInfoResp.newBuilder();
        builder.setAfter(roleNumInfo.getNum());
        builder.setAddExp(0);
        return builder.build();
    }
}
