package com.pdool.game.core.res.impl;


import cn.hutool.core.map.MapUtil;
import com.pdool.common.entity.CardPieceEntity;
import com.pdool.game.core.res.AbsRes;
import com.pdool.game.logic.service.CardService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 碎片资源
 *  装备 配置：2_equipId_num
 *  2_111_1
 */
@Component("2")
public class CardPieceRes extends AbsRes<Map<Integer,Integer>> {
    @Resource
    CardService cardService;
    @Override
    public boolean canAdd(long roleId, Map<Integer, Integer> add) {
        //  TODO
        return true;
    }
    @Override
    public boolean canDec(long roleId, Map<Integer, Integer> cost) {
        Map<Integer, CardPieceEntity> allPiece = cardService.getAllCardPiece(roleId);
        for (Map.Entry<Integer, Integer> entry : cost.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (!allPiece.containsKey(key)) {
                return false;
            }
            CardPieceEntity cardPieceEntity = allPiece.get(key);
            if (cardPieceEntity.getNum() < value){
                return false;
            }
        }
        return false;
    }

    @Override
    public boolean add(long roleId, Map<Integer, Integer> add) {
        Map<Integer, CardPieceEntity> allPiece = cardService.getAllCardPiece(roleId);
        for (Map.Entry<Integer, Integer> entry : add.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();

            CardPieceEntity orDefault = allPiece.getOrDefault(key, new CardPieceEntity());
            orDefault.setRoleId(roleId);
            orDefault.setNum(orDefault.getNum() + value);
            orDefault.setPieceId(key);
            cardService.updateCardPiece(allPiece,orDefault);
        }

        cardService.pushCardPiece(roleId);

        return true;
    }

    @Override
    public boolean dec(long roleId, Map<Integer, Integer> cost) {
        Map<Integer, CardPieceEntity> allPiece = cardService.getAllCardPiece(roleId);
        for (Map.Entry<Integer, Integer> entry : cost.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (!allPiece.containsKey(key)) {
                return false;
            }
            CardPieceEntity cardPieceEntity = allPiece.get(key);
            if (cardPieceEntity.getNum() < value){
                return false;
            }

            cardPieceEntity.setNum(cardPieceEntity.getNum() -value);
            cardService.updateCardPiece(allPiece,cardPieceEntity);
        }
        cardService.pushCardPiece(roleId);
        return true;
    }

    @Override
    public Map<Integer, Integer> merge(long roleId, String[] add, Map<Integer, Integer> oldMap) {
        if (oldMap == null){
            oldMap = MapUtil.newHashMap();
        }
        int equipId = Integer.parseInt(add[1]);
        int num = Integer.parseInt(add[2]);
        int oldNum = oldMap.getOrDefault(equipId,0);
        int newNum = oldNum + num;
        oldMap.put(equipId,newNum);
        return oldMap;
    }
}
