package com.pdool.game.logic.handler.guide;

import com.pdool.common.constants.MsgId;
import com.pdool.common.entity.GuideInfoEntity;
import com.pdool.common.msg.guide.SetGuideInfoReq;
import com.pdool.common.msg.guide.SetGuideInfoResp;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.service.GuideService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 保存引导信息
 */
@MsgHandler(MsgId.SET_GUIDE_INFO)
@Component
public class SetGuideInfoHandler6004 implements IHandler<SetGuideInfoReq, SetGuideInfoResp> {
    @Resource
    GuideService guideService;
    @Override
    public SetGuideInfoResp handle(Long roleId, SetGuideInfoReq req) {
        int guideId = req.getGuideId();
        int stepId = req.getStepId();

        GuideInfoEntity guideInfoEntity = guideService.queryGuideInfo(roleId);

        guideInfoEntity.setGuideId(guideId);
        guideInfoEntity.setStepId(stepId);

        guideService.updateGuideInfo(guideInfoEntity);

        SetGuideInfoResp build = SetGuideInfoResp.newBuilder().setGuideId(guideId).setStepId(stepId).build();
        return build;
    }
}
