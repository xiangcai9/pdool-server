package com.pdool.game.logic.handler.card.group;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.card.UpdateCardGroupReq;
import com.pdool.common.msg.common.NullResp;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.service.CardGroupService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 更新卡组
 */
@MsgHandler(MsgId.UPDATE_CARD_GROUP)
@Component
public class UpdateCardGroupHandler implements IHandler<UpdateCardGroupReq, NullResp> {
    @Resource
    CardGroupService cardGroupService;
    @Override
    public NullResp handle(Long roleId, UpdateCardGroupReq req) {

        cardGroupService.addCard2Group(roleId,req);

        return null;
    }
}
