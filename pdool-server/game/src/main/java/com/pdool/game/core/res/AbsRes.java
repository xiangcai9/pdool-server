package com.pdool.game.core.res;

/**
 * @author 香菜
 */
public abstract class AbsRes<T> {
    //  检查是否可以增加
    public abstract boolean canAdd(long roleId, T add);
    //  检查是否可以扣除
    public abstract boolean canDec(long roleId, T cost);
    //  增加
    public abstract boolean add(long roleId, T add);
    //  减少
    public abstract boolean dec(long roleId, T cost);
    //  合并
    public abstract T merge(long roleId, String[] add, T old);
}
