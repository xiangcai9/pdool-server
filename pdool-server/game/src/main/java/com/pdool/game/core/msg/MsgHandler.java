package com.pdool.game.core.msg;

import java.lang.annotation.*;

/**
 * msg注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MsgHandler {

    int value();
}
