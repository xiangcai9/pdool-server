package com.pdool.game.core.msg;

import com.pdool.common.service.room.IRoomService;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/**
 * 房间管理器
 */
@Data
@Component
@Slf4j
public class RoomRemoteMgr implements InitializingBean, ApplicationRunner {
    private static RoomRemoteMgr instance;

    public static RoomRemoteMgr getInstance() {
        return instance;
    }

    @Override
    public void afterPropertiesSet() {
        instance = this;
    }

    @DubboReference(url = "${room.dubbo}")
    IRoomService roomService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        String s = roomService.sayHello("  tell me ok ");
        log.error("----------->  {} ", s);
    }
}
