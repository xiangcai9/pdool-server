package com.pdool.game.logic.enums;

/**
 * 卡牌类型
 */
public enum CardTypeEnum {
    /**角色 */
    ROLE,
    /**道具 */
    ITEM,
    /**武器 */
    WEAPON,
    /**坐骑 */
    CAR
}
