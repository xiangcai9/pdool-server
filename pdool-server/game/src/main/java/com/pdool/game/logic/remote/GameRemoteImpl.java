package com.pdool.game.logic.remote;

import com.pdool.common.dto.FightResultDto;
import com.pdool.common.service.game.IGameService;
import com.pdool.game.logic.mgr.UserMgr;
import com.pdool.game.logic.service.FightResultService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.WebSocketSession;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 远程调用接口
 */
@DubboService
@Slf4j
public class GameRemoteImpl implements IGameService {
    @Resource
    FightResultService fightResultService;

    @Override
    public void trans2Client(long roleId, byte[] buffer) {

        WebSocketSession webSocketSession = UserMgr.getInstance().getSessionMap().get(roleId);
        try {
            if (webSocketSession != null && webSocketSession.isOpen()) {
                synchronized (webSocketSession) {
                    BinaryMessage binaryMessage = new BinaryMessage(buffer);
                    webSocketSession.sendMessage(binaryMessage);
                }
            }
        } catch (IOException e) {
            log.error("",e);
        }
    }

    @Override
    public void trans2Game( Object msg) {
        //  TODO:game 处理
        try {
            if (msg instanceof  FightResultDto){
                fightResultService.onFightEnd((FightResultDto) msg);
            }
        } catch (Exception e) {
            log.error("",e);
        }
    }

    @Override
    public String sayHello() {
        log.error("msg from room ,game remote receive  ");
        return "game";
    }


}
