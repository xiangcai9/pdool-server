package com.pdool.game;

import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableDubbo
@EnableCaching
@EnableAsync
@EnableScheduling
@MapperScan("com.pdool.common.mapper.mapper")
@ComponentScan("com.pdool")
@Slf4j
public class GameApplication {


    public static void main(String[] args) {
        SpringApplication.run(GameApplication.class, args);

        log.error("--------------------启动成功------------------");
    }

}
