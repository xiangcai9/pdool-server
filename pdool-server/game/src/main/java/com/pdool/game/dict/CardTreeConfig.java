package com.pdool.game.dict;

import com.pdool.common.dict.CardTreeDictVo;
import com.pdool.game.core.cache.AbsConfigCache;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 卡牌树配置
 */
@Component
public class CardTreeConfig extends AbsConfigCache<Integer, CardTreeDictVo> {
    public CardTreeConfig() {
        super("卡牌树表_card_tree.xlsx", 3);
    }

    @Override
    protected CardTreeDictVo convert2Value(Map<Integer, String> rowData) {
        int id = Integer.parseInt(rowData.get(1));
        int exp = Integer.parseInt(rowData.get(2));
        int before = id - 1;
        if (getDataMap().containsKey(before)) {
            CardTreeDictVo cardTreeDictVo = getDataMap().get(before);
            exp = exp - cardTreeDictVo.getExp();
        }
        String drop = rowData.get(3);
        String reward = rowData.get(4);
        CardTreeDictVo build = CardTreeDictVo.builder()
                .id(id)
                .exp(exp)
                .drop(drop)
                .reward(reward)
                .build();
        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        String s = rowData.get(1);
        return Integer.valueOf(s);
    }
}
