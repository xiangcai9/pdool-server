package com.pdool.game.core.res.impl;

import com.pdool.game.core.res.AbsRes;
import com.pdool.game.logic.enums.NumTypeEnum;
import com.pdool.game.logic.service.RankService;
import com.pdool.game.logic.service.RoleService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 积分资源
 * 配置：1_num
 * @author 香菜
 */
@Component("1")
public class ScoreRes extends AbsRes<Integer> {
    @Resource
    RoleService roleService;
    @Resource
    RankService rankService;

    @Override
    public boolean canAdd(long roleId, Integer add) {
        return true;
    }

    @Override
    public boolean canDec(long roleId, Integer cost) {
        return true;
    }

    @Override
    public boolean add(long roleId, Integer add) {
        if (add == 0) {
            return true;
        }
        Long aLong = roleService.updateRoleNum(roleId, NumTypeEnum.SCORE, add);
        roleService.pushSingleNumChange(roleId,NumTypeEnum.SCORE);
        rankService.updateScore(roleId, Math.toIntExact(aLong));
        return true;
    }

    @Override
    public  boolean dec(long roleId, Integer add) {
        if (add == 0) {
            return true;
        }

        Long aLong = roleService.updateRoleNum(roleId, NumTypeEnum.SCORE, -add);
        roleService.pushSingleNumChange(roleId,NumTypeEnum.SCORE);
        rankService.updateScore(roleId, Math.toIntExact(aLong));
        return true;
    }

    @Override
    public Integer merge(long roleId, String[] add, Integer old) {
        if (old == null){
            old = 0;
        }
        int addNum = Integer.parseInt(add[1]);

        return addNum + old;
    }
}
