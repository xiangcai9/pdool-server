package com.pdool.game.logic.handler.card;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.card.CardPieceResp;
import com.pdool.common.msg.common.NullReq;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.service.CardService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 查询卡牌碎片
 */
@Component
@MsgHandler(MsgId.QUERY_CARD_PIECE)
public class QueryCardPieceHandler implements IHandler<NullReq, CardPieceResp> {
    @Resource
    CardService cardService;
    @Override
    public CardPieceResp handle(Long roleId, NullReq req) {
        cardService.pushCardPiece(roleId);
        return null;
    }
}