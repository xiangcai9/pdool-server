package com.pdool.game.logic.service;

import com.pdool.common.constants.MsgId;
import com.pdool.common.entity.GuideInfoEntity;
import com.pdool.common.msg.guide.GuideInfoSTC;
import com.pdool.game.cache.GuideInfoCache;
import com.pdool.game.util.SessionUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 引导逻辑处理类
 */
@Service
public class GuideService {
    @Resource
    GuideInfoCache guideInfoCache;


    public GuideInfoEntity queryGuideInfo(long roleId){
        GuideInfoEntity guideInfoEntity = guideInfoCache.queryRoleOne(roleId);
        return guideInfoEntity;
    }

    public void updateGuideInfo(GuideInfoEntity guideInfoEntity){
        guideInfoCache.updateUserGuide(guideInfoEntity);
    }


    public void pushGuideInfo(long roleId){
        GuideInfoEntity guideInfoEntity = queryGuideInfo(roleId);

        GuideInfoSTC.Builder builder = GuideInfoSTC.newBuilder()
                .setGuideId(guideInfoEntity.getGuideId())
                .setStepId(guideInfoEntity.getStepId());

        SessionUtil.pushMsg(roleId,MsgId.PUSH_GUIDE_INFO,builder.build());
    }

}
