package com.pdool.game.logic.handler.card.group;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.card.GroupInfoResp;
import com.pdool.common.msg.common.NullReq;
import com.pdool.game.cache.CardGroupCache;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.domain.entity.vo.CardGroupVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 查询卡组
 */
@MsgHandler(MsgId.GROUP_INFO)
@Component
public class QueryCardGroupHandler implements IHandler<NullReq, GroupInfoResp> {
    @Autowired
    CardGroupCache cardCache;

    @Override
    public GroupInfoResp handle(Long roleId, NullReq req) {
        Map<Integer, CardGroupVo> allCardGroup = cardCache.getAllCardGroup(roleId);
        GroupInfoResp.Builder builder = GroupInfoResp.newBuilder();

        for (CardGroupVo value : allCardGroup.values()) {
            GroupInfoResp.GroupInfo.Builder itemBuilder = GroupInfoResp.GroupInfo.newBuilder();
            itemBuilder.addAllCardId(value.getCardGroupList());
            itemBuilder.setGroupIndex(value.getEntity().getGroupIndex());
            if (value.getEntity().getGroupName() != null){
                itemBuilder.setGroupName(value.getEntity().getGroupName());
            }
            builder.addGroupInfoList(itemBuilder);
        }
        return builder.build();
    }
}
