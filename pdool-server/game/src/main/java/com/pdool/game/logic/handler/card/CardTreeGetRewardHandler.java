package com.pdool.game.logic.handler.card;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.card.CardTreeGetRewardReq;
import com.pdool.common.msg.common.NullResp;
import com.pdool.game.core.msg.IHandler;
import com.pdool.game.core.msg.MsgHandler;
import com.pdool.game.logic.service.CardGroupService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 领取卡牌树奖励
 */
@MsgHandler(MsgId.CARD_TREE_GET_REWARD)
@Component
public class CardTreeGetRewardHandler implements IHandler<CardTreeGetRewardReq, NullResp> {
    @Resource
    CardGroupService cardGroupService;
    @Override
    public NullResp handle(Long roleId, CardTreeGetRewardReq reward) {
        int lv = reward.getLv();
        cardGroupService.getCardTreeReward(roleId,lv);

        return null;
    }
}
