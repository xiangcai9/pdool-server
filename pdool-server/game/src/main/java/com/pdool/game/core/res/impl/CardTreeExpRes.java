package com.pdool.game.core.res.impl;

import com.pdool.common.entity.RoleNumInfoEntity;
import com.pdool.common.msg.card.CardTreeInfoResp;
import com.pdool.game.core.res.AbsRes;
import com.pdool.game.logic.enums.NumTypeEnum;
import com.pdool.game.logic.service.RoleService;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 卡牌树经验资源
 * 配置：3_num
 *
 * @author 香菜
 */
@Component("3")
public class CardTreeExpRes extends AbsRes<Integer> {
    @Resource
    RoleService roleService;

    @Override
    public boolean canAdd(long roleId, Integer add) {
        return true;
    }

    @Override
    public boolean canDec(long roleId, Integer cost) {
        return false;
    }

    @Override
    public boolean add(long roleId, Integer add) {
        roleService.updateRoleNum(roleId, NumTypeEnum.CARD_TREE_EXP, add);

        RoleNumInfoEntity roleNumInfo = roleService.getRoleNumInfo(roleId, NumTypeEnum.CARD_TREE_EXP);
        CardTreeInfoResp.Builder builder = CardTreeInfoResp.newBuilder();
        builder.setAfter(roleNumInfo.getNum());
        builder.setAddExp(add);

        roleService.pushNumInfo(roleId);
        return true;
    }

    @Override
    public boolean dec(long roleId, Integer add) {
        return true;
    }

    @Override
    public Integer merge(long roleId, String[] add, Integer old) {
        if (old == null) {
            old = 0;
        }
        int addNum = Integer.parseInt(add[1]);
        return addNum + old;
    }
}
