package com.pdool.game.core.res;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
/**
 * 资源类型管理类
 * @author 香菜
 */
@Component
public class ResTypeManager implements InitializingBean{
    @Autowired
    public Map<String,AbsRes> allResMap;

    private static ResTypeManager instance;

    public static ResTypeManager getInstance() {
        return instance;
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        instance = this;
    }
}
