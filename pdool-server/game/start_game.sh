 docker run -itd \
  -e ENV=test \
  -p 18090:18090 \
  -p 20800:20800 \
  --name game \
  -v /data/excel:/excel \
  -v /data/game/log:/app/log \
  --restart=always game:20230705
