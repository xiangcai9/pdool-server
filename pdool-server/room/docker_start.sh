 docker run -itd \
  -e ENV=test \
  -p 9090:9090 \
  -p 9999:9999 \
  --name room \
  -v /data/excel:/excel \
  -v /home/docker/room:/app/log \
  --restart=always room:0.0.1-SNAPSHOT
