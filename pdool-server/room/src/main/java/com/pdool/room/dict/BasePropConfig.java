package com.pdool.room.dict;


import com.pdool.common.dict.BasePropVo;
import com.pdool.room.core.AbsConfigCache;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class BasePropConfig extends AbsConfigCache<String, BasePropVo> {

    public BasePropConfig() {
        super("基础属性_base_config.xlsx");
    }

    @Override
    protected BasePropVo convert2Value(Map<Integer, String> rowData) {
        String id = rowData.get(1);
        String value = rowData.get(2);
        BasePropVo build = BasePropVo.builder().id(id).value(value).build();
        return build;
    }

    @Override
    protected String convert2Key(Map<Integer, String> rowData) {
        String s = rowData.get(1);
        return s;
    }
}
