package com.pdool.room.fight.effect;

import com.pdool.common.constants.Constant;
import com.pdool.room.context.RoomContext;
import com.pdool.room.fight.FightUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class CardFightVo {
    //  所属角色
    private long roleId;
    //  卡牌id
    private int cardId;
    //   战力
    private int fight;
    //  临时的战斗力，每回合计算
    private int tmpFight;
    //  能量消耗
    private int cost;
    //  卡牌类型
    private int cardType;
    //  所在战场
    private int areaId = Constant.AREA_POOL; //
    //  回合 出站的回合 -1 表示手牌
    private int round = -1;
    //  阵营
    private int pos;
    //  是否在阵容内
    private boolean isValid = true;
    //  是否虚拟牌（）
    private boolean isFakeCard = false;

    /**
     *
     * @param fight
     * @param roomContext
     * @param isNotice 是否通知
     */
    public void setFight(int fight, RoomContext roomContext, boolean... isNotice) {
        this.fight = fight;
        if (isNotice.length == 0) {
            push(roomContext);
        }
    }

    public void setTmpFight(int fight,RoomContext roomContext,boolean ... isNotice){
        this.tmpFight = fight;
        if (isNotice.length == 0) {
            push(roomContext);
        }
    }

    public void push(RoomContext roomContext){
        FightUtil.getInstance().pushCardFight(roomContext, this);
    }
}
