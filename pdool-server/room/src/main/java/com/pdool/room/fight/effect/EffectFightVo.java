package com.pdool.room.fight.effect;

import com.pdool.common.dict.CardEffectVo;
import com.pdool.room.enums.EffectCreatorType;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class EffectFightVo {

    private CardEffectVo effectConfigVo;
    private EffectCreatorType type = EffectCreatorType.CARD;
    //  创建效果的回合
    private int createRound;
    //  哪张牌的效果
    private CardFightVo cardFightVo;

    private int areaId;

    public EffectFightVo(CardEffectVo effectConfigVo) {
        this.effectConfigVo = effectConfigVo;

    }
}
