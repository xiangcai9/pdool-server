package com.pdool.room.enums;

public enum CardTypeEnum {
    //  角色卡
    ROLE(1),
    //  武器卡
    WEAPON(2),
    // 坐骑
    RIDE(3),
    //  所有
    ALL(4)
    ;

    private int type;

    CardTypeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
