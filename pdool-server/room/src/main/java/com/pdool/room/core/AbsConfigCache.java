package com.pdool.room.core;

import com.alibaba.excel.EasyExcel;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

import javax.annotation.Resource;
import java.io.File;
import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Slf4j
public abstract class AbsConfigCache<KEY, VALUE> implements ApplicationRunner {
    /**
     * excel的路径（docker挂载）
     */
    private String filePath;
    /**
     * 表头的行数
     */
    private int headRowNum = 3;
    /**
     * 读第几个sheet
     */
    private int sheetNo = 0;


    private Map<KEY, VALUE> dataMap = new HashMap<>();

    @Resource
    private ExcelConfig excelConfig;

    public AbsConfigCache(String filePath) {
        this.filePath = filePath;
    }

    public AbsConfigCache(String filePath, int dataIndex) {
        this.filePath = filePath;
        this.headRowNum = dataIndex;
    }

    public AbsConfigCache(String filePath, int dataIndex, int sheetNo) {
        this.filePath = filePath;
        this.headRowNum = dataIndex;
        this.sheetNo = sheetNo;
    }

    private Class<VALUE> domainClass;

    @Override
    public void run(ApplicationArguments args) {

        try {
            // 子类直接获取范型C类型
            this.domainClass = (Class<VALUE>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                    .getActualTypeArguments()[1];
            loadData();

            String simpleName = this.getClass().getSimpleName();

            DictMgr.getInstance().getDictMap().put(simpleName, this);
        } catch (Exception e) {
            log.error("", e);
        }

    }

    public void loadData() {
        String filePath = excelConfig.getExcelPath() + File.separator + this.filePath;
        List<Map<Integer, String>> listMap = EasyExcel.read(filePath).sheet(sheetNo).headRowNumber(headRowNum).doReadSync();
        Map<KEY, VALUE> dataMap = new HashMap<>();
        int rowNum = headRowNum;
        try {
            for (int i = 0; i < listMap.size(); i++) {
                rowNum = headRowNum + i + 1;
                Map<Integer, String> rowData = listMap.get(i);
                KEY key = convert2Key(rowData);
                VALUE value = convert2Value(rowData);
                dataMap.put(key, value);
            }
        } catch (Exception e) {
            log.error("fileName : {} ,rowNum :  {}",filePath,rowNum);
            throw new RuntimeException(e);
        }
        this.dataMap = dataMap;

        log.error("fileName : {} 加载完成",filePath);
    }

    public VALUE getByKey(KEY key) {
        return dataMap.get(key);
    }

    public Map<KEY, VALUE> getAll() {
        return dataMap;
    }

    //  TODO 可以优化，生成一个默认的convert ,使用easyexcel的方式
    protected abstract VALUE convert2Value(Map<Integer, String> rowData);
//    {
//        VALUE bean = BeanUtil.toBean(rowData, this.domainClass);
//        return bean;
//    }

    protected abstract KEY convert2Key(Map<Integer, String> rowData);


}
