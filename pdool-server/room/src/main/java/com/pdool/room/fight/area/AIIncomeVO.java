package com.pdool.room.fight.area;

import com.pdool.room.fight.effect.CardFightVo;
import lombok.Data;

@Data
public class AIIncomeVO {
    CardFightVo cardFightVo;
    int areaId;
    int income;

}
