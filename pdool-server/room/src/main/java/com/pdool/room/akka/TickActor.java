package com.pdool.room.akka;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import com.pdool.room.mgr.RoomMgr;

import java.time.Duration;

//@Component
//@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)

/**
 * 定时器actor
 */
public class TickActor extends AbstractActor{

    @Override
    public Receive createReceive() {
        ActorSystem system = this.context().system();
        return receiveBuilder()
                .matchEquals("tick", s -> sendTickToRoom())
                .matchEquals("init",s -> {
                    system.scheduler().schedule(Duration.ZERO, Duration.ofSeconds(1), this.self(), "tick", system.dispatcher(), ActorRef.noSender());
                }).build();
    }

    private void sendTickToRoom() {
        RoomMgr.getInstance().checkRoom();
        for (ActorRef value : RoomMgr.getInstance().roomMap.values()) {
            value.tell("tick",this.self());
        }
    }

}
