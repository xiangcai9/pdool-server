package com.pdool.room.fight.area;

import lombok.Data;

/**
 * 手牌区域信息
 */
@Data
public class HandAreaInfo extends AreaInfo {

    public HandAreaInfo(long roleId, int areaId) {
        super(roleId, areaId);
    }
}