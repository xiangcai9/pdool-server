package com.pdool.room.fight.area;

import com.pdool.room.fight.effect.CardFightVo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 区域信息
 */
@Data
@NoArgsConstructor
public class AreaInfo {
    private long roleId;
    private int areaId;
    private List<CardFightVo> cardList = new ArrayList<>();

    public AreaInfo(long roleId,int areaId) {
        this.areaId = areaId;
        this.roleId = roleId;
    }
}