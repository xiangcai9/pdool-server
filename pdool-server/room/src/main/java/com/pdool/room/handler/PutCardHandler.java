package com.pdool.room.handler;

import com.pdool.common.constants.Constant;
import com.pdool.common.constants.ErrorCode;
import com.pdool.common.constants.MsgId;
import com.pdool.common.dict.CardEffectVo;
import com.pdool.common.dict.CardInfoConfigVo;
import com.pdool.common.msg.fight.PutCardReq;
import com.pdool.common.msg.fight.PutCardResp;
import com.pdool.room.context.RoleFightInfo;
import com.pdool.room.context.RoomContext;
import com.pdool.room.core.MsgHandler;
import com.pdool.room.dict.BasePropConfig;
import com.pdool.room.dict.CardConfig;
import com.pdool.room.dict.CardEffectConfig;
import com.pdool.room.fight.area.FightAreaInfo;
import com.pdool.room.fight.effect.CardFightVo;
import com.pdool.room.fight.effect.EffectFightVo;
import com.pdool.room.mgr.RoomMgr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * 出牌
 */
@MsgHandler(MsgId.PUT_CARD)
@Component
@Slf4j
public class PutCardHandler implements IRoomHandler<PutCardReq> {
    @Autowired
    BasePropConfig basePropConfig;
    @Autowired
    CardConfig cardConfig;
    @Autowired
    CardEffectConfig cardEffectConfig;


    @Override
    public void handle(RoomContext roomContext, long roleId, PutCardReq putCardReq) {
        int areaMaxCardNum = Integer.parseInt(basePropConfig.getByKey(Constant.PROP_AREA_MAX_CARD_NUM).getValue());
        int cardId = putCardReq.getCardId();
        RoleFightInfo roleFightInfo = roomContext.getMemberMap().get(roleId);
        //  做一些检查 检查能量，检查牌是不是已经用了
        List<FightAreaInfo> areaInfoList = roleFightInfo.getAreaInfoList();
        int zoneId = putCardReq.getZoneId();
        FightAreaInfo areaInfo = areaInfoList.get(zoneId);
        Optional<CardFightVo> first = roleFightInfo.getHandAreaInfo().getCardList().stream().filter(e -> e.getCardId() == putCardReq.getCardId()).findFirst();
        if (areaInfo.getCardList().size() >= areaMaxCardNum) {
            PutCardResp build = PutCardResp.newBuilder().setCardId(cardId).setResult(1).setErrorCode(ErrorCode.AREA_CARD_FULL).build();
            RoomMgr.getInstance().sendMsg2Client(roleId, MsgId.PUT_CARD, build);
            return;
        }
        first.ifPresent(cardFightVo -> {
            if (!roleFightInfo.decEnergy(cardFightVo.getCost())) {
                PutCardResp build = PutCardResp.newBuilder().setCardId(cardId).setResult(1).setErrorCode(ErrorCode.ENERGY_NOT_ENOUGH).build();
                RoomMgr.getInstance().sendMsg2Client(roleId, MsgId.PUT_CARD, build);
                return;
            }

            cardFightVo.setRound(roomContext.getCurRound());
            cardFightVo.setAreaId(putCardReq.getZoneId());
            roleFightInfo.getLastCardFightVo().add(cardFightVo);
            areaInfo.getCardList().add(cardFightVo);

            //  创建效果，加入到列表中
            CardInfoConfigVo byKey = cardConfig.getByKey(cardId);
            Integer effectId = byKey.getEffect();
            if (effectId != null) {
                List<EffectFightVo> effectList = roomContext.getEffectList();
                CardEffectVo cardEffectConfigVo = cardEffectConfig.getByKey(effectId);
                if (cardEffectConfigVo == null) {
                    log.error("------->  effectId {}  cardId  {}", effectId, cardId);
                }
                EffectFightVo effectFightVo = new EffectFightVo(cardEffectConfigVo);
                effectFightVo.setCardFightVo(cardFightVo);
                effectFightVo.setAreaId(cardFightVo.getAreaId());
                effectFightVo.setCreateRound(roomContext.getCurRound());
                effectList.add(effectFightVo);
            }

            roleFightInfo.getHandAreaInfo().getCardList().remove(cardFightVo);

            sendMsg2Other(cardFightVo, roomContext);
        });
    }

    private void sendMsg2Other(CardFightVo cardFightVo, RoomContext roomContext) {
        PutCardResp.Builder builder = PutCardResp.newBuilder();
        builder.setRoleId(cardFightVo.getRoleId());
        builder.setAreaId(cardFightVo.getAreaId());
        builder.setCardId(cardFightVo.getCardId());
        RoomMgr.getInstance().sendMsg2Room(roomContext, MsgId.PUT_CARD, builder.build());
    }

}
