package com.pdool.room.enums;

public enum CampTypeEnum {
    //  自己
    SELF(2),
    //  敌人
    ENEMY(1),
    //  所有
    ALL(3)
    ;

    private int type;

    CampTypeEnum(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
