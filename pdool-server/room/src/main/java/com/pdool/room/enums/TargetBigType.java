package com.pdool.room.enums;

public enum TargetBigType {
    AREA(1),
    CARD(2),
    SPECIAL(3),
    ;

    private int targetType;

    TargetBigType(int targetType) {
        this.targetType = targetType;
    }

    public int getTargetType() {
        return targetType;
    }

    public static TargetBigType targetType(int i) {
        for (TargetBigType value : TargetBigType.values()) {
            if (value.targetType == i) {
                return value;
            }
        }
        return null;
    }
}
