package com.pdool.room.enums;

public enum ActionPahseEnum {
    // 1 出牌后
    AFTER_CARD(1),
    //  回合结束后
    ROUND_END(2),
    ;

    private final int phase;

    ActionPahseEnum(int phase) {
        this.phase = phase;
    }

    public int getPhase() {
        return phase;
    }
}
