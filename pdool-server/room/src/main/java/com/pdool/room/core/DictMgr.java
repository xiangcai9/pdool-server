package com.pdool.room.core;

import lombok.Data;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
@Component
public class DictMgr implements InitializingBean {

    Map<String, AbsConfigCache> dictMap = new HashMap<>();

    static DictMgr instance;
    public static DictMgr getInstance(){
        return instance;
    }
    public void reload(String dictName){
        dictMap.get(dictName).loadData();
    }

    @Override
    public void afterPropertiesSet() {
        instance = this;
    }
}
