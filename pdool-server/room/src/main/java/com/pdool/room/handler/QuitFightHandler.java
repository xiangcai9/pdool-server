package com.pdool.room.handler;

import com.pdool.common.constants.MsgId;
import com.pdool.common.dto.FightResultDto;
import com.pdool.common.msg.common.NullReq;
import com.pdool.room.context.RoleFightInfo;
import com.pdool.room.context.RoomContext;
import com.pdool.room.core.MsgHandler;
import com.pdool.room.mgr.RoomMgr;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;

/**
 * 放弃战斗
 */
@Component
@MsgHandler(MsgId.QUIT_FIGHT)
@Slf4j
public class QuitFightHandler implements IRoomHandler<NullReq> {

    @Override
    public void handle(RoomContext roomContext, long roleId, NullReq putCardReq) {
        Map<Long, RoleFightInfo> memberMap = roomContext.getMemberMap();
        FightResultDto fightResultDto = new FightResultDto();
        fightResultDto.setRoomId(roomContext.getRoomId());
        fightResultDto.setReason(1);
        fightResultDto.setRoleList(new ArrayList<>(memberMap.keySet()));
        for (RoleFightInfo value : memberMap.values()) {
            //  设置
            if (value.getRoleId() != roleId) {
                fightResultDto.getWinnerSet().add(value.getRoleId());
            } else {
                fightResultDto.getLoserSet().add(value.getRoleId());
            }
        }
        //   结算
        RoomMgr.getInstance().getGameService().trans2Game(fightResultDto);
        RoomMgr.getInstance().stopActor(roomContext.getRoomId());
    }
}
