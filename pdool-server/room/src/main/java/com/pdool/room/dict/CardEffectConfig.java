package com.pdool.room.dict;

import com.pdool.common.constants.Constant;
import com.pdool.common.dict.CardEffectVo;
import com.pdool.room.core.AbsConfigCache;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;

@Component
public class CardEffectConfig extends AbsConfigCache<Integer, CardEffectVo> {
    public CardEffectConfig() {
        super("卡牌效果表_card_effect.xlsx");
    }

    @Override
    protected CardEffectVo convert2Value(Map<Integer, String> rowData) {

        int id = Integer.parseInt(rowData.get(1));
        int actionPhase = Integer.parseInt(rowData.get(4));
        int roundPos = Integer.parseInt(rowData.get(5));
        Integer activeRound = null;
        if (StringUtils.hasText(rowData.get(6))){
            activeRound = Integer.parseInt(rowData.get(6));
        }

        int campType = Integer.parseInt(rowData.get(7));
        int areaType = Integer.parseInt(rowData.get(8));
        String[] targetCondition = rowData.get(9).split(Constant.SPLIT_LINE);
        int exAction = Integer.parseInt(rowData.get(10));
        int actionTarget = Integer.parseInt(rowData.get(11));
        Integer num = null;
        if (StringUtils.hasText(rowData.get(12))){
            num = Integer.parseInt(rowData.get(12));
        }

        CardEffectVo build = CardEffectVo.builder().effectId(id)
                .roundPos(roundPos)
                .activeRound(activeRound)
                .campType(campType)
                .areaType(areaType)
                .targetCondition(targetCondition)
                .exAction(exAction)
                .actionTarget(actionTarget)
                .actionNum(num)
                .actionPhase(actionPhase)
                .build();

        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        String s = rowData.get(1);
        return Integer.valueOf(s);
    }
}
