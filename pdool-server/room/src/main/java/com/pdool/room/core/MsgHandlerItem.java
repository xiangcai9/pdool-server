package com.pdool.room.core;

import com.google.protobuf.Parser;
import com.pdool.room.handler.IRoomHandler;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MsgHandlerItem {
    private int msgId;
    private Parser in;
    private IRoomHandler handler;
}
