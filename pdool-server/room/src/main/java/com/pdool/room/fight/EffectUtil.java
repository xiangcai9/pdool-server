package com.pdool.room.fight;

import com.alibaba.fastjson2.JSON;
import com.pdool.common.constants.Constant;
import com.pdool.room.context.RoleFightInfo;
import com.pdool.room.context.RoomContext;
import com.pdool.room.fight.area.AreaInfo;
import com.pdool.room.fight.area.FightAreaInfo;
import com.pdool.room.fight.effect.CardFightVo;
import com.pdool.room.fight.effect.EffectFightVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class EffectUtil {
    static EffectUtil instance;

    public EffectUtil() {
        instance = this;
    }

    public static EffectUtil getInstance() {
        return instance;
    }

    /**
     * 是否效果可用
     *
     * @param effectFightVo
     * @param roomContext
     * @return
     */
    public boolean canActiveEffect(EffectFightVo effectFightVo, RoomContext roomContext) {
        int roundPos = effectFightVo.getEffectConfigVo().getRoundPos();
        int createRound = effectFightVo.getCreateRound();
        Integer activeRound = effectFightVo.getEffectConfigVo().getActiveRound();
        if (activeRound == null) {
            return true;
        }
        if (roundPos == Constant.ROUND_POS_2) {
            if (createRound + 1 != roomContext.getCurRound()) {
                return false;
            }
        } else if (roundPos == Constant.ROUND_POS_4) {
            if (activeRound != roomContext.getCurRound()) {
                return false;
            }
        } else if (roundPos == Constant.ROUND_POS_7) {
            if (activeRound == roomContext.getCurRound()) {
                return false;
            }
        }
        return true;
    }

    /**
     * 效果生效
     *
     * @param effectFightVo 效果对象
     * @param targetList    目标列表  卡牌(CardFightVo)，玩家（roleFightInfo），区域(AreaInfo)）
     * @param roomContext   room上下文
     */
    public void doEffect(EffectFightVo effectFightVo, List<Object> targetList, RoomContext roomContext) {
        int actionTarget = effectFightVo.getEffectConfigVo().getActionTarget();
        switch (actionTarget) {
            case Constant.ACTION_TARGET_1://    1-当前卡
                doActionOnSingleCard(effectFightVo, roomContext, effectFightVo.getCardFightVo(), targetList);
                break;
            case Constant.ACTION_TARGET_2://    2-目标卡
                doActionOnTarget2(targetList, roomContext, effectFightVo);
                break;
            case Constant.ACTION_TARGET_3://    3-目标区域
                doActionOnTarget3(targetList, roomContext, effectFightVo);
                break;
            case Constant.ACTION_TARGET_4://    4-玩家能量
                doActionOnTarget4(targetList, effectFightVo);
            default:
                break;
        }


    }

    public boolean removeEffect(EffectFightVo effectFightVo, RoomContext roomContext) {
        int roundPos = effectFightVo.getEffectConfigVo().getRoundPos();
        int createRound = effectFightVo.getCreateRound();
        //  当前回合
        if (roundPos == Constant.ROUND_POS_1) {
            return true;
            //  下回合
        } else if (roundPos == Constant.ROUND_POS_2) {
            if (createRound + 1 == roomContext.getCurRound()) {
                return true;
            }
            //  指定回合
        } else if (roundPos == Constant.ROUND_POS_4) {
            if (createRound == roomContext.getCurRound()) {
                return true;
            }
        }

        return false;
    }

    //    2-目标卡
    private void doActionOnTarget2(List<Object> targetList, RoomContext roomContext, EffectFightVo effectFightVo) {
        for (Object o : targetList) {
            CardFightVo cardFightVo = (CardFightVo) o;
            doActionOnSingleCard(effectFightVo, roomContext, cardFightVo, targetList);
        }
    }

    private void doActionOnTarget3(List<Object> targetList, RoomContext roomContext, EffectFightVo effectFightVo) {
        for (Object o : targetList) {
            AreaInfo areaInfo = (AreaInfo) o;
            doActionOnArea(effectFightVo, roomContext, areaInfo);
        }
    }

    private void doActionOnTarget4(List<Object> targetList, EffectFightVo effectFightVo) {

        if (targetList == null) {
            //  TODO 下回合能量锁定
        } else {
            for (Object o : targetList) {
                if (o instanceof RoleFightInfo) {
                    RoleFightInfo roleFightInfo = (RoleFightInfo) o;
                    roleFightInfo.setAddEnergy(effectFightVo.getEffectConfigVo().getActionNum() + roleFightInfo.getAddEnergy());
                }
            }

        }
    }

    //  区域效果
    private void doActionOnArea(EffectFightVo effectFightVo, RoomContext roomContext, AreaInfo areaInfo) {
        int exAction = effectFightVo.getEffectConfigVo().getExAction();
        switch (exAction) {
            case Constant.EX_ACTION_TARGET_1:// 战力+-
                ActionUtil.getInstance().action1_area((FightAreaInfo) areaInfo, effectFightVo.getEffectConfigVo().getActionNum(), roomContext);
                break;
            case Constant.EX_ACTION_TARGET_2:// 战力翻倍
                ActionUtil.getInstance().action2_area((FightAreaInfo) areaInfo,roomContext);
                break;
            case Constant.EX_ACTION_TARGET_13:
                ActionUtil.getInstance().action13_area(areaInfo, roomContext);
                break;
            case Constant.EX_ACTION_TARGET_21:// 临时战力+-
                ActionUtil.getInstance().action21_area((FightAreaInfo) areaInfo, effectFightVo.getEffectConfigVo().getActionNum(), roomContext);
                break;
        }

    }

    //  单张卡牌效果
    private void doActionOnSingleCard(EffectFightVo effectFightVo, RoomContext roomContext, CardFightVo cardFightVo, List<Object> targetList) {
        if (cardFightVo == null) {
            log.error("  effectFightVo {}, targetList{}", JSON.toJSONString(effectFightVo), JSON.toJSONString(targetList));
            return;
        }
        int exAction = effectFightVo.getEffectConfigVo().getExAction();
        try {
            switch (exAction) {
                case Constant.EX_ACTION_TARGET_1:// 战力+-
                    ActionUtil.getInstance().action1(cardFightVo, effectFightVo, roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_2:// 战力翻倍
                    ActionUtil.getInstance().action2(cardFightVo, roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_3:// 移动到指定战场
                    ActionUtil.getInstance().action3(cardFightVo);
                    break;
                case Constant.EX_ACTION_TARGET_5:// 回到手牌（6初始）
                    ActionUtil.getInstance().action5(cardFightVo, roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_7:// 将指定卡牌移到场外
                    ActionUtil.getInstance().action7(cardFightVo, roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_8:// 消耗锁定（0）
                    ActionUtil.getInstance().action8(cardFightVo, effectFightVo.getEffectConfigVo().getActionNum());
                    break;
                case Constant.EX_ACTION_TARGET_10://    增加能量
                    ActionUtil.getInstance().action10(cardFightVo.getRoleId(), roomContext, effectFightVo.getEffectConfigVo().getActionNum());
                    break;
                case Constant.EX_ACTION_TARGET_11://    针对当前卡-存在单位数量*action对应参数
                    ActionUtil.getInstance().action11(cardFightVo, targetList, effectFightVo.getEffectConfigVo().getActionNum(), roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_14://   14 针对当前卡增加战力-存在则增加action对应参数
                    ActionUtil.getInstance().action14(cardFightVo, targetList, effectFightVo.getEffectConfigVo().getActionNum(), roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_15://  15 针对当前卡战力翻倍-存在则增加action对应参数
                    ActionUtil.getInstance().action15(cardFightVo, targetList,roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_21://  21临时战力+-（每回合开始时重置，针对多轮计算）
                    ActionUtil.getInstance().action21(cardFightVo, effectFightVo, roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_23://  针对当前卡增加临时战力-存在单位数量*action对应参数 （每回合开始时重置，针对多轮计算）
                    ActionUtil.getInstance().action23(cardFightVo, targetList, effectFightVo.getEffectConfigVo().getActionNum(), roomContext);
                    break;
                case Constant.EX_ACTION_TARGET_24://  针对当前卡增加临时战力-存在则增加action对应参数 （每回合开始时重置，针对多轮计算）
                    ActionUtil.getInstance().action24(cardFightVo, targetList, effectFightVo.getEffectConfigVo().getActionNum(), roomContext);
                    break;
                default:
                    log.error("wrong config  cardId :{}  effectId {} ", cardFightVo.getCardId(), effectFightVo.getEffectConfigVo().getEffectId());
            }
        } catch (Exception e) {
            log.error("effectFightVo {},cardFightVo {}  targetList{}", JSON.toJSONString(effectFightVo), JSON.toJSONString(cardFightVo), JSON.toJSONString(targetList));
            log.error("", e);
        }
    }
}
