package com.pdool.room.context;

import com.pdool.room.fight.FightUtil;
import com.pdool.room.fight.area.FightAreaInfo;
import com.pdool.room.fight.area.HandAreaInfo;
import com.pdool.room.fight.effect.CardFightVo;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 玩家战斗信息
 */
@Data
public class RoleFightInfo {
    long roleId;
    String roomId;
    //  阵营
    int pos;
    //  通用能量
    int roundEnergy;
    //  额外能量
    int addEnergy;
    //  当前轮的卡牌(有bug，可能是一个列表)
    List<CardFightVo> lastCardFightVo = new ArrayList<>();
    //  所有的卡牌
    List<CardFightVo> cardList = new ArrayList<>();
    //  战斗区域信息
    List<FightAreaInfo> areaInfoList = new ArrayList<>();
    //  手牌区域
    HandAreaInfo handAreaInfo;


    public boolean decEnergy(int count) {
        boolean b = canDecEnergy(count);
        if (!b) {
            return false;
        }
        if (addEnergy >= count) {
            addEnergy = addEnergy - count;
        } else {
            roundEnergy = addEnergy + roundEnergy - count;
            addEnergy = 0;
        }
        FightUtil.getInstance().pushEnergy(this);
        return true;
    }

    public boolean canDecEnergy(int count) {
        return addEnergy + roundEnergy >= count;
    }

    public void setRoundEnergy(int roundEnergy) {
        this.roundEnergy = roundEnergy;
        FightUtil.getInstance().pushEnergy(this);
    }

    public void setAddEnergy(int addEnergy) {
        this.addEnergy = addEnergy;
        FightUtil.getInstance().pushEnergy(this);
    }
}
