package com.pdool.room.fight;

import com.pdool.room.context.RoleFightInfo;
import com.pdool.room.context.RoomContext;
import com.pdool.room.fight.area.FightAreaInfo;
import com.pdool.room.fight.effect.EffectFightVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class AreaEffectUtil {
    static AreaEffectUtil instance;

    public AreaEffectUtil() {
        instance = this;
    }

    public static AreaEffectUtil getInstance() {
        return instance;
    }


    public void doAreaEffect(RoomContext roomContext) {
//        log.error("doAreaEffect   curRound {} ", roomContext.getCurRound());
        List<EffectFightVo> areaEffectList = roomContext.getAreaEffectList();
        for (EffectFightVo effectFightVo : areaEffectList) {
            int effectId = effectFightVo.getEffectConfigVo().getEffectId();
            //10001		双方手牌中的卡牌+2000战力
            //10005		此区域的卡牌+5000战力。
            //10006		此区域的卡牌-2000战力
            if (effectId == 10001 || effectId == 10005 || effectId == 10006) {
                for (RoleFightInfo value : roomContext.getMemberMap().values()) {
                    if (value.getLastCardFightVo().size() != 0) {
                        value.getLastCardFightVo().forEach(e -> {
                            if (e.getAreaId() == effectFightVo.getAreaId()) {
                                ActionUtil.getInstance().action1(e, effectFightVo, roomContext);
                            }
                        });
//                        log.error("doAreaEffect   curRound {} cardID {},effectId {} ", roomContext.getCurRound(), value.getLastCardFightVo().getCardId(), effectId);
                    }
                }
//            10015		你在此处没有牌的回合+1能量。
            } else if (effectId == 10015) {
                for (RoleFightInfo value : roomContext.getMemberMap().values()) {
                    int areaId = effectFightVo.getAreaId();
                    FightAreaInfo fightAreaInfo = value.getAreaInfoList().get(areaId);
                    if (fightAreaInfo.getCardList().size() == 0) {
                        ActionUtil.getInstance().action10(value.getRoleId(), roomContext, effectFightVo.getEffectConfigVo().getActionNum());
                    } /*else {
                        removeList.add(effectFightVo);
                    }*/
                }
                //若你在此区域仅有1张卡牌，使其+5000战力。
            } else if (effectId == 10017) {
                for (RoleFightInfo value : roomContext.getMemberMap().values()) {
                    int areaId = effectFightVo.getAreaId();
                    FightAreaInfo fightAreaInfo = value.getAreaInfoList().get(areaId);
                    if (fightAreaInfo.getCardList().size() == 1) {
                        if (!fightAreaInfo.getWaitRemoveList().contains(effectFightVo)) {
                            ActionUtil.getInstance().action1_area(fightAreaInfo, effectFightVo.getEffectConfigVo().getActionNum(), roomContext);
                            fightAreaInfo.getWaitRemoveList().add(effectFightVo);
                        }
                    } else if (fightAreaInfo.getCardList().size() > 1) {
                        ActionUtil.getInstance().action1_area(fightAreaInfo, effectFightVo.getEffectConfigVo().getActionNum() * -1, roomContext);
                    }
                }
                //  当你在此区域放置卡牌时，其战力翻倍。
            } else if (10021 == effectId) {
                for (RoleFightInfo value : roomContext.getMemberMap().values()) {
                    if (value.getLastCardFightVo().size() != 0) {
                        value.getLastCardFightVo().forEach(e -> {
                            if (e.getAreaId() == effectFightVo.getAreaId()) {
                                ActionUtil.getInstance().action2(e, roomContext);
                            }
                        });
                    }
                }
                //当前战场道具战力+1k
                //当前战场武器战力+1k
                //当前战场坐骑战力+1k
                //当前战场角色战力-1k
            } else if (10030 == effectId || 10031 == effectId || 10032 == effectId || 10033 == effectId) {
                String[] targetCondition = effectFightVo.getEffectConfigVo().getTargetCondition();
                int targetType = Integer.parseInt(targetCondition[1]);
                for (RoleFightInfo value : roomContext.getMemberMap().values()) {

                    if (value.getLastCardFightVo().size() != 0) {
                        value.getLastCardFightVo().forEach(e -> {
                            if (e.getAreaId() == effectFightVo.getAreaId()) {
                                if (e.getCardType() == targetType) {
                                    ActionUtil.getInstance().action1(e, effectFightVo, roomContext);
                                }
                            }
                        });
//                        log.error("doAreaEffect   curRound {} cardID {},effectId {} ", roomContext.getCurRound(), value.getLastCardFightVo().getCardId(), effectId);
                    }
                }
            }
        }
    }

}
