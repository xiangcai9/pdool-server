package com.pdool.room.enums;

public enum EffectActiveType {
    OPEN(2),
    ALWAYS(3),
    ;
    private int type;

    EffectActiveType(int type) {
        this.type = type;
    }
    public static EffectActiveType getEffectActiveType(int i) {
        for (EffectActiveType value : EffectActiveType.values()) {
            if (value.type == i) {
                return value;
            }
        }
        return null;
    }
}
