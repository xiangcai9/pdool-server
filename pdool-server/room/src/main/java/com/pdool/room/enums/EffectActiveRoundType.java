package com.pdool.room.enums;

public enum EffectActiveRoundType {
    //  当前回合
    CURRENT(1),
    //  下回合
    NEXT(2),
    //  全部回合
    ALL(3),
    //  指定回合
    CERTAIN(4),
    //  非指定回合
    EXCLUDE_ROUND(7),
    ;
    private int type;

    public int getType() {
        return type;
    }

    EffectActiveRoundType(int type) {
        this.type = type;
    }
    public static EffectActiveRoundType getEffectActiveType(int i) {
        for (EffectActiveRoundType value : EffectActiveRoundType.values()) {
            if (value.type == i) {
                return value;
            }
        }
        return null;
    }
}
