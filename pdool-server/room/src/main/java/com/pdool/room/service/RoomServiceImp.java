package com.pdool.room.service;

import com.pdool.common.dto.room.ClientRoomMsg;
import com.pdool.common.dto.room.CreateRoomInfo;
import com.pdool.common.service.room.IRoomService;
import com.pdool.room.mgr.RoomMgr;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * room远程服务
 */
@DubboService
@Slf4j
public class RoomServiceImp implements IRoomService {
    @Override
    public String createRoom(CreateRoomInfo createRoomInfo) {
        String roomId = RoomMgr.getInstance().createRoom(createRoomInfo);
        return roomId;
    }

    @Override
    public String sayHello(String echo) {
        log.error("receiveMsg " + echo);
        String s = RoomMgr.getInstance().getGameService().sayHello();
        return echo;
    }

    @Override
    public void trans2Room(String roomId, ClientRoomMsg msg) {
        try {
            RoomMgr.getInstance().sendMsg2Room(roomId, msg);
        } catch (Exception e) {
            log.error("",e);
        }
    }

}
