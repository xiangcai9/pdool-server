package com.pdool.room.handler;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.fight.ReadyFightReq;
import com.pdool.room.context.RoleFightInfo;
import com.pdool.room.context.RoomContext;
import com.pdool.room.core.MsgHandler;
import org.springframework.stereotype.Component;

/**
 * 准备好了
 */
@MsgHandler(MsgId.READY_FIGHT)
@Component
public class ReadyFightHandler implements IRoomHandler<ReadyFightReq> {

    @Override
    public void handle(RoomContext roomContext, long roleId, ReadyFightReq req) {
        RoleFightInfo roleFightInfo = roomContext.getMemberMap().get(roleId);
        roomContext.readyFight(roleFightInfo.getPos());
    }
}
