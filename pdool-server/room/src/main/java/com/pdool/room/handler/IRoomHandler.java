package com.pdool.room.handler;

import com.pdool.room.context.RoomContext;

public interface IRoomHandler<T> {
    public void handle(RoomContext roomContext, long roleId, T req);
}
