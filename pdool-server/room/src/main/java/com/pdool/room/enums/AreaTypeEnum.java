package com.pdool.room.enums;

public enum AreaTypeEnum {
    //0-当前
    CURRENT(0),
    //1-相邻
    NEIGHBOR(1),
    //2左侧区域
    LEFT(2),
    //3右侧区域
    RIGHT(3),
    //4全部区域
    ALL(4),
    //5除当前之外的所有区域
    EXCLUDE(5),
    //6 手牌区域
    HAND(6)
    ;

    private int area;

    AreaTypeEnum(int area) {
        this.area = area;
    }

    public int getArea() {
        return area;
    }
}
