package com.pdool.room.dict;

import com.pdool.common.dict.AreaEffectVo;
import com.pdool.room.core.AbsConfigCache;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Map;
@Component
public class AreaEffectConfig  extends AbsConfigCache<Integer, AreaEffectVo> {
    public AreaEffectConfig() {
        super("区域效果表_area_effect.xlsx");
    }

    @Override
    protected AreaEffectVo convert2Value(Map<Integer, String> rowData) {
        int id = Integer.parseInt(rowData.get(1));
        AreaEffectVo.AreaEffectVoBuilder builder = AreaEffectVo.builder();
        if (StringUtils.hasText(rowData.get(2)) ){
            int conditionId = Integer.parseInt(rowData.get(2));
            builder.effectId(conditionId);
        }
        AreaEffectVo build = builder.id(id).build();
        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        String s = rowData.get(1);
        return Integer.valueOf(s);
    }
}
