//package com.pdool.room.fight;
//
//import com.pdool.common.constants.Constant;
//import com.pdool.common.msg.fight.Fight;
//import com.pdool.common.msg.fight.PutCardReq;
//import com.pdool.room.context.RoleFightInfo;
//import com.pdool.room.context.RoomContext;
//import com.pdool.room.dict.BasePropConfig;
//import com.pdool.room.fight.area.AIIncomeVO;
//import com.pdool.room.fight.area.FightAreaInfo;
//import com.pdool.room.fight.effect.CardFightVo;
//import com.pdool.room.handler.PutCardHandler;
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.util.ArrayList;
//import java.util.List;
//
//@Component
//@Slf4j
//public class AIUtilBak {
//    static AIUtilBak instance;
//
//    public AIUtilBak() {
//        instance = this;
//    }
//
//    public static AIUtilBak getInstance() {
//        return instance;
//    }
//
//    @Resource
//    BasePropConfig basePropConfig;
//
//    @Resource
//    PutCardHandler putCardHandler;
//
//
//    int maxValue;
//    List<AIIncomeVO> finalList;
//
//    List<FightAreaInfo> simulateHandList;
//
//
//    public void backTrace(int energy, RoleFightInfo roleFightInfo, List<FightAreaInfo> simulateHandList, int income, List<AIIncomeVO> planList) {
//        if (energy <= 0) {
//            return;
//        }
//        if (手牌用尽) {
//            return;
//        }
//        if (所有的区域都满了) {
//            return;
//        }
//        for (CardFightVo cardFightVo : roleFightInfo.getHandAreaInfo().getCardList()) {
//            if (不是已经出牌的) {
//                出牌加入到区域中;
//                income += 计算收益;
//                从手牌中移除;
//                backTrace(energy - cardFightVo.getCost(), roleFightInfo, simulateHandList, income, planList);
//                出牌删除区域;
//                income -= 收益;
//                加回手牌
//            }
//        }
//    }
//
//
//}
