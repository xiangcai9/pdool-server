package com.pdool.room.handler;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.fight.NextRoundReq;
import com.pdool.room.context.RoleFightInfo;
import com.pdool.room.context.RoomContext;
import com.pdool.room.core.MsgHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 下一轮
 */
@Component
@Slf4j
@MsgHandler(MsgId.NEXT_ROUND)
public class SkipRoundHandler implements IRoomHandler<NextRoundReq> {
    @Override
    public void handle(RoomContext roomContext, long roleId, NextRoundReq req) {
        RoleFightInfo roleFightInfo = roomContext.getMemberMap().get(roleId);
        roomContext.skipRound(roleFightInfo.getPos());
    }
}
