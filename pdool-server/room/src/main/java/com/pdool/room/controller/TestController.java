package com.pdool.room.controller;

import akka.actor.ActorSystem;
import com.pdool.common.service.game.IGameService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试controller
 */
@RestController
public class TestController {
    @DubboReference(check = false,url = "${game.dubbo}")
    IGameService helloService;

    @Autowired
    private ActorSystem actorSystem;
    @GetMapping("/rest")
    public String sayHello(){

        return "s";
    }

    @GetMapping("/test")
    public void test(){
//        RoomMgr.getInstance().createRoom(1,2);
    }
}
