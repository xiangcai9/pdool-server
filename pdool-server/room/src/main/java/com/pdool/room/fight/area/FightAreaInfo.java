package com.pdool.room.fight.area;

import com.pdool.room.fight.effect.EffectFightVo;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 区域信息
 */
@Data
public class FightAreaInfo extends AreaInfo {
    //  临时的战斗力，每回合计算
    private long tmpFight;
    //  pos -fight 额外的战力
    private Long addFight = 0L;
    //  汇总之后的
    private Long sumFight = 0L;

    private List<EffectFightVo> waitRemoveList = new ArrayList<>();
    public FightAreaInfo(long roleId, int areaId) {
        super(roleId,areaId);
    }
}