package com.pdool.room.fight;

import com.pdool.common.constants.Constant;
import com.pdool.common.msg.fight.PutCardReq;
import com.pdool.room.context.RoleFightInfo;
import com.pdool.room.context.RoomContext;
import com.pdool.room.dict.BasePropConfig;
import com.pdool.room.fight.area.FightAreaInfo;
import com.pdool.room.fight.effect.CardFightVo;
import com.pdool.room.handler.PutCardHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
@Component
@Slf4j
public class AIUtil {
    static AIUtil instance;

    public AIUtil() {
        instance = this;
    }

    public static AIUtil getInstance() {
        return instance;
    }

    @Resource
    BasePropConfig basePropConfig;

    @Resource
    PutCardHandler putCardHandler;

    public void aiPutCard(RoomContext roomContext, RoleFightInfo roleFightInfo){
        //  1、选择zone
        int areaId = selectNoFullZone(roleFightInfo);
        if (areaId == -1){
            roomContext.skipRound(roleFightInfo.getPos());
            return;
        }
        //  2、选择卡牌
        int cardId = selectOneCard(roleFightInfo);
        if (cardId == -1){
            roomContext.skipRound(roleFightInfo.getPos());
            return;
        }
        PutCardReq.Builder builder = PutCardReq.newBuilder();
        builder.setCardId(cardId);
        builder.setZoneId(areaId);
        putCardHandler.handle(roomContext, roleFightInfo.getRoleId(), builder.build());

//        log.error(" ai put  card  areaId {} ,cardId {} ",areaId,cardId);

        roomContext.skipRound(roleFightInfo.getPos());
    }

    private int selectOneCard(RoleFightInfo roleFightInfo) {
        int energy = roleFightInfo.getAddEnergy() + roleFightInfo.getRoundEnergy();
        int closestCardId = -1;
        int gap = Integer.MAX_VALUE;
        for (CardFightVo cardFightVo : roleFightInfo.getHandAreaInfo().getCardList()) {
            if (!cardFightVo.isValid()){
                continue;
            }
            if (cardFightVo.getAreaId() != -1) {
                continue;
            }
            int cost = cardFightVo.getCost();
            if (energy >= cost && energy- cost < gap){
                closestCardId = cardFightVo.getCardId();
                gap = energy - cost;
            }
        }
        return closestCardId;
    }

    private int selectNoFullZone(RoleFightInfo roleFightInfo) {
        List<FightAreaInfo> areaInfoList = roleFightInfo.getAreaInfoList();
        int maxCount = Integer.parseInt(basePropConfig.getByKey(Constant.PROP_AREA_MAX_CARD_NUM).getValue());
        for (int i = 0; i < areaInfoList.size(); i++) {
            FightAreaInfo areaInfo = areaInfoList.get(i);
            if (areaInfo.getCardList().size() < maxCount){
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        if (Integer.MAX_VALUE > 11972660888L){
            System.out.println("晓宇");
        }else {
            System.out.println("大于");
        }
    }
}
