package com.example.client;

import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.match.MatchReq;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class Test {
    public static void main(String[] args) {
        MatchReq.Builder builder = MatchReq.newBuilder();
        builder.setRoomType(1);
        byte[] byteArray = builder.build().toByteArray();
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES + byteArray.length);
        buffer.putInt(MsgId.MATCH);
        buffer.put(byteArray);
        System.out.println(Arrays.toString(buffer.array()));
    }
}
