package com.example.client;

import com.google.protobuf.GeneratedMessageV3;
import com.pdool.common.constants.MsgId;
import com.pdool.common.msg.fight.PutCardReq;
import com.pdool.common.msg.fight.ReadyFightReq;
import com.pdool.common.msg.match.MatchReq;
import lombok.extern.slf4j.Slf4j;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.nio.ByteBuffer;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author: tanghaizhi
 * @CreateTime: 2022/10/13 14:13
 * @Description:
 */
@Slf4j
public class WebsocketRunClient extends WebSocketClient{
    /**
     * websocket连接名称
     */
    private String wsName;


    public WebsocketRunClient(URI serverUri,String wsName) {
        super(serverUri);
        this.wsName = wsName;


    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        log.info("[websocket {}] Websocket客户端连接成功",wsName);
        MatchReq.Builder builder = MatchReq.newBuilder();
        builder.setRoomType(1);
        builder.setCardGroupIndex(1);
        sendMsg(MsgId.MATCH,builder.build());

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                ReadyFightReq.Builder builder1 = ReadyFightReq.newBuilder();
                sendMsg(MsgId.READY_FIGHT,builder1.build());
            }
        },10*1000);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                PutCardReq.Builder builder1 = PutCardReq.newBuilder();
                builder1.setZoneId(0);
                builder1.setCardId(2);
                sendMsg(MsgId.PUT_CARD,builder1.build());
            }
        },15*1000);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                PutCardReq.Builder builder1 = PutCardReq.newBuilder();
                builder1.setZoneId(0);
                builder1.setCardId(2);
                sendMsg(MsgId.PUT_CARD,builder1.build());
            }
        },45*1000);
    }

    @Override
    public void onMessage(String msg) {
        log.info("[websocket {}] 收到String消息：{}",wsName,msg);
    }

    @Override
    public void onMessage(ByteBuffer bytes) {
        log.info("[websocket {}] 收到ByteBuffer消息：{}",wsName);
    }


    @Override
    public void onClose(int code, String reason, boolean remote) {
        log.info("[websocket {}] Websocket客户端关闭",wsName);
        System.out.println("Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: " + reason);
    }

    @Override
    public void onError(Exception e) {
        log.info("[websocket {}] Websocket客户端出现异常, 异常原因为：{}",wsName,e.getMessage());
    }

    public void sendMsg(int msgId, GeneratedMessageV3 msg){
        byte[] byteArray = msg.toBuilder().build().toByteArray();
        ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES + byteArray.length);
        buffer.putInt(msgId);
        buffer.put(byteArray);
        send(buffer.array());
    }
}
