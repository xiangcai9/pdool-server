package com.pdool.common.mapper.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pdool.common.entity.GuideInfoEntity;
import com.pdool.common.mapper.mapper.GuideInfoMapper;
import com.pdool.common.mapper.service.IGuideInfoService;
import org.springframework.stereotype.Service;

@Service
public class GuideInfoDaoImpl extends ServiceImpl<GuideInfoMapper, GuideInfoEntity> implements IGuideInfoService {

}
