package com.pdool.common.mapper.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pdool.common.entity.CardInfoEntity;
import com.pdool.common.mapper.mapper.CardInfoMapper;
import com.pdool.common.mapper.service.ICardInfoService;
import org.springframework.stereotype.Service;

@Service
public class CardInfoDaoImpl extends ServiceImpl<CardInfoMapper, CardInfoEntity> implements ICardInfoService {

}
