// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: fight.proto

package com.pdool.common.msg.fight;

public interface CardFightInfoSTCOrBuilder extends
    // @@protoc_insertion_point(interface_extends:proto.CardFightInfoSTC)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   * 角色id
   * </pre>
   *
   * <code>int64 ownerId = 1;</code>
   * @return The ownerId.
   */
  long getOwnerId();

  /**
   * <pre>
   * 战场id  -1 是手牌
   * </pre>
   *
   * <code>int32 areaId = 2;</code>
   * @return The areaId.
   */
  int getAreaId();

  /**
   * <pre>
   *卡牌id
   * </pre>
   *
   * <code>int32 cardId = 3;</code>
   * @return The cardId.
   */
  int getCardId();

  /**
   * <pre>
   * 能量消耗
   * </pre>
   *
   * <code>int32 cost = 4;</code>
   * @return The cost.
   */
  int getCost();

  /**
   * <pre>
   *战力
   * </pre>
   *
   * <code>int32 fight = 5;</code>
   * @return The fight.
   */
  int getFight();

  /**
   * <pre>
   * 是否复制牌 true是复制牌 false 常规卡牌
   * </pre>
   *
   * <code>bool isFake = 6;</code>
   * @return The isFake.
   */
  boolean getIsFake();

  /**
   * <pre>
   * 是否在阵容内 true在阵容中 false移除阵容
   * </pre>
   *
   * <code>bool isValid = 7;</code>
   * @return The isValid.
   */
  boolean getIsValid();
}
