package com.pdool.common.mapper.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pdool.common.entity.RoleNumInfoEntity;
import com.pdool.common.mapper.mapper.RoleNumInfoMapper;
import com.pdool.common.mapper.service.IRoleNumInfoService;
import org.springframework.stereotype.Service;

@Service
public class RoleNumInfoDaoImpl extends ServiceImpl<RoleNumInfoMapper, RoleNumInfoEntity> implements IRoleNumInfoService {

}
