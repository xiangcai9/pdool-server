package com.pdool.common.mapper.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pdool.common.entity.StatPointEntity;
import com.pdool.common.mapper.mapper.StatPointMapper;
import com.pdool.common.mapper.service.IStatPointService;
import org.springframework.stereotype.Service;

@Service
public class StatPointDaoImpl extends ServiceImpl<StatPointMapper, StatPointEntity> implements IStatPointService {

}
