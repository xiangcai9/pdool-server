// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: rank.proto

package com.pdool.common.msg.rank;

public interface ScoreRankRespOrBuilder extends
    // @@protoc_insertion_point(interface_extends:proto.ScoreRankResp)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>repeated .proto.ScoreRankResp.RankItem rankList = 1;</code>
   */
  java.util.List<com.pdool.common.msg.rank.ScoreRankResp.RankItem> 
      getRankListList();
  /**
   * <code>repeated .proto.ScoreRankResp.RankItem rankList = 1;</code>
   */
  com.pdool.common.msg.rank.ScoreRankResp.RankItem getRankList(int index);
  /**
   * <code>repeated .proto.ScoreRankResp.RankItem rankList = 1;</code>
   */
  int getRankListCount();
  /**
   * <code>repeated .proto.ScoreRankResp.RankItem rankList = 1;</code>
   */
  java.util.List<? extends com.pdool.common.msg.rank.ScoreRankResp.RankItemOrBuilder> 
      getRankListOrBuilderList();
  /**
   * <code>repeated .proto.ScoreRankResp.RankItem rankList = 1;</code>
   */
  com.pdool.common.msg.rank.ScoreRankResp.RankItemOrBuilder getRankListOrBuilder(
      int index);
}
