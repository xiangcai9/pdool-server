package com.pdool.common.mapper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pdool.common.entity.GuideInfoEntity;

public interface IGuideInfoService extends IService<GuideInfoEntity> {
}

