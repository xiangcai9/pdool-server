package com.pdool.common.constants;

public class ErrorCode {

    //  未知错误
    public static final String UNKNOWN_ERROR = "999";
    //  卡牌树等级不足
    public static final String CARD_TREE_LV_NO_ENOUGH = "1000";
    //  卡牌树奖励已经领取
    public static final String CARD_TREE_HAS_GET = "1001";
    //  不存在的卡牌组
    public static final String CARD_GROUP_NOT_EXIST = "1002";
    //  卡牌组中存在相同的卡
    public static final String CARD_GROUP_SAME_CARD = "1003";
    // 卡牌数量过多
    public static final String CARD_GROUP_TOO_MANY= "1004";
    //  区域已经满了
    public static final String AREA_CARD_FULL= "1005";
    //  能量不够,出牌失败
    public static final String ENERGY_NOT_ENOUGH= "1006";
    //  需组成12张一组的卡组才能参加比赛哦
    public static final String FIGHT_CARD_GROUP_NOT_ENOUGH= "1007";
    //  卡牌不存在
    public static final String NOT_EXIST_CARD = "1008";
    //  卡牌等级已经最高了
    public static final String CARD_HAS_MAX_LV = "1009";
    //  不存在的卡牌组
    public static final String NOT_EXIST_GROUP_INDEX = "1010";
    //  错误的卡组名字
    public static final String WRONG_GROUP_NAME = "1011";
}
