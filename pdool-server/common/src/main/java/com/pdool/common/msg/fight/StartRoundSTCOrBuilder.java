// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: fight.proto

package com.pdool.common.msg.fight;

public interface StartRoundSTCOrBuilder extends
    // @@protoc_insertion_point(interface_extends:proto.StartRoundSTC)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   *  回合
   * </pre>
   *
   * <code>int32 round = 1;</code>
   * @return The round.
   */
  int getRound();

  /**
   * <pre>
   * 倒计时
   * </pre>
   *
   * <code>int32 leftSec = 2;</code>
   * @return The leftSec.
   */
  int getLeftSec();

  /**
   * <pre>
   * 最大回合
   * </pre>
   *
   * <code>int32 maxRound = 3;</code>
   * @return The maxRound.
   */
  int getMaxRound();
}
