package com.pdool.common.mapper.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pdool.common.entity.CardGroupEntity;
import com.pdool.common.mapper.mapper.CardGroupMapper;
import com.pdool.common.mapper.service.ICardGroupService;
import org.springframework.stereotype.Service;

@Service
public class CardGroupDaoImpl extends ServiceImpl<CardGroupMapper, CardGroupEntity> implements ICardGroupService {

}
