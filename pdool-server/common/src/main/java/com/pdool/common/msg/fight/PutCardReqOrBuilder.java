// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: fight.proto

package com.pdool.common.msg.fight;

public interface PutCardReqOrBuilder extends
    // @@protoc_insertion_point(interface_extends:proto.PutCardReq)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <pre>
   *卡牌id
   * </pre>
   *
   * <code>int32 cardId = 1;</code>
   * @return The cardId.
   */
  int getCardId();

  /**
   * <pre>
   * 战场id
   * </pre>
   *
   * <code>int32 zoneId = 2;</code>
   * @return The zoneId.
   */
  int getZoneId();
}
