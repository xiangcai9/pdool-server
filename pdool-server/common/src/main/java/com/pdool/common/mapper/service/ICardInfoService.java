package com.pdool.common.mapper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pdool.common.entity.CardInfoEntity;

public interface ICardInfoService extends IService<CardInfoEntity> {
}

