package com.pdool.common.mapper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pdool.common.entity.RoleNumInfoEntity;

public interface IRoleNumInfoService extends IService<RoleNumInfoEntity> {
}

