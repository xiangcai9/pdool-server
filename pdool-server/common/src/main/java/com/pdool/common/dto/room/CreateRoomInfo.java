package com.pdool.common.dto.room;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class CreateRoomInfo implements Serializable {
    private List<Integer> areaInfo;
    private String roomId;
    private List<RoleFightInitInfo> fightInfoList = new ArrayList<>();

}
