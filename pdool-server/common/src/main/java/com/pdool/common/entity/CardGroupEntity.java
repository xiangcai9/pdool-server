package com.pdool.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("card_group")
@Data
public class CardGroupEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Long roleId;
    private int groupIndex;
    private String cardGroup;
    private String groupName;
}
