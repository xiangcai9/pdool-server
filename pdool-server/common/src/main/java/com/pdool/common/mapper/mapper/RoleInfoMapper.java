package com.pdool.common.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdool.common.entity.RoleInfoEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface RoleInfoMapper extends BaseMapper<RoleInfoEntity> {
    @Select("select * from role_info where name = #{userName} and pwd = #{pwd}")
    List<RoleInfoEntity> findAllByNameAndPwd(String userName, String pwd);
    @Select("select count(1) from role_info where name = #{userName}")
    int existsByName(String userName);

    @Select("select max(role_id) from role_info ")
    Integer findMaxRoleId();


    @Select("select role_id ,name from role_info")
    List<Map<String,Object>> findAllRoleName();
}
