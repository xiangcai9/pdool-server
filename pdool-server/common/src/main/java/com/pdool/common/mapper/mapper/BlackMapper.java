package com.pdool.common.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Date;

@Mapper
public interface BlackMapper extends BaseMapper {
    @Update("replace into black (role_id,save_time) values (#{roleId},#{date})")
    boolean insertOne(long roleId, Date date);

    @Select("select count(1 ) from black where role_id = #{roleId}")
    Integer isExist(Long roleId);
    }
