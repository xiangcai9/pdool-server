package com.pdool.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("role_num_info")
@Data
public class RoleNumInfoEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Long roleId;
    private int numType;
    private Long num = 0L;
    private String extend;

}
