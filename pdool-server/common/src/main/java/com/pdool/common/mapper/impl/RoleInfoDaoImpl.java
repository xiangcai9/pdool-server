package com.pdool.common.mapper.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pdool.common.entity.RoleInfoEntity;
import com.pdool.common.mapper.mapper.RoleInfoMapper;
import com.pdool.common.mapper.service.IRoleInfoService;
import org.springframework.stereotype.Service;

@Service
public class RoleInfoDaoImpl extends ServiceImpl<RoleInfoMapper, RoleInfoEntity> implements IRoleInfoService {
}
