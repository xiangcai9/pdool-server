package com.pdool.common.dict;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class CardInfoConfigVo {
    int id;
    String name;
    int cardType;
    int cost;
    int fight;
    int lv;
    Map<Integer,String> levelCost;
    Integer effect;
    int limit;


}
