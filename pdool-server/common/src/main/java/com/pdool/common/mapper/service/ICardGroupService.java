package com.pdool.common.mapper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pdool.common.entity.CardGroupEntity;

public interface ICardGroupService extends IService<CardGroupEntity> {
}

