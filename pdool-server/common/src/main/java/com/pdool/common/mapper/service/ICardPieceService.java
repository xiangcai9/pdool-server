package com.pdool.common.mapper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pdool.common.entity.CardPieceEntity;

public interface ICardPieceService extends IService<CardPieceEntity> {
}

