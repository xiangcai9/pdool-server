package com.pdool.common.dto.room;

import lombok.Data;

import java.io.Serializable;

@Data
public class ClientRoomMsg implements Serializable {
    private long roleId;
    private int msgId;
    private String roomId;
    private byte[] msg;
}
