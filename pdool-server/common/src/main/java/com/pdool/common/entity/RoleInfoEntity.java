package com.pdool.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("role_info")
public class RoleInfoEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Long roleId;
    private Integer roleType;
    private String name;
    private String pwd;
    private String head;
    private Date createTime;
    private Date updateTime;

}
