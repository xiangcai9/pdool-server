package com.pdool.common.service.room;

import com.pdool.common.dto.room.ClientRoomMsg;
import com.pdool.common.dto.room.CreateRoomInfo;

public interface IRoomService {

    String createRoom(CreateRoomInfo createRoomInfo);
    String sayHello(String echo);


    void trans2Room(String roleId, ClientRoomMsg clientRoomMsg);

}
