package com.pdool.common.dict;

import cn.hutool.core.lang.WeightRandom;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class DropOutVo {
    private String tagId;
    List<WeightRandom.WeightObj<String>> weightObjs = new ArrayList<>();
}
