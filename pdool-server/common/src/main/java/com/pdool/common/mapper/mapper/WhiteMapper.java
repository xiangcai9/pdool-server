package com.pdool.common.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface WhiteMapper extends BaseMapper {

    @Select("select distinct role_id from white")
    List<Long> selectAllWhite();
}
