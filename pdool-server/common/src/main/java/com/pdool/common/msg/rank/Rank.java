// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: rank.proto

package com.pdool.common.msg.rank;

public final class Rank {
  private Rank() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_proto_ScoreRankResp_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_proto_ScoreRankResp_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_proto_ScoreRankResp_RankItem_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_proto_ScoreRankResp_RankItem_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\nrank.proto\022\005proto\"\207\001\n\rScoreRankResp\022/\n" +
      "\010rankList\030\001 \003(\0132\035.proto.ScoreRankResp.Ra" +
      "nkItem\032E\n\010RankItem\022\016\n\006roleId\030\001 \001(\003\022\014\n\004na" +
      "me\030\002 \001(\t\022\r\n\005score\030\003 \001(\005\022\014\n\004rank\030\004 \001(\005B\035\n" +
      "\031com.pdool.common.msg.rankP\001b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_proto_ScoreRankResp_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_proto_ScoreRankResp_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_proto_ScoreRankResp_descriptor,
        new java.lang.String[] { "RankList", });
    internal_static_proto_ScoreRankResp_RankItem_descriptor =
      internal_static_proto_ScoreRankResp_descriptor.getNestedTypes().get(0);
    internal_static_proto_ScoreRankResp_RankItem_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_proto_ScoreRankResp_RankItem_descriptor,
        new java.lang.String[] { "RoleId", "Name", "Score", "Rank", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
