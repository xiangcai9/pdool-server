package com.pdool.common.dict;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BasePropVo {
    private String id;
    private String value;
}
