package com.pdool.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("guide_info")
@Data
public class GuideInfoEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Long roleId;
    private int guideId;
    private int stepId;
}
