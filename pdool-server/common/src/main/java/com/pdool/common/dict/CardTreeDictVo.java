package com.pdool.common.dict;

import lombok.Builder;
import lombok.Data;

@Data
@Builder

public class CardTreeDictVo {
    private int id;
    private int exp;
    private String drop;
    private String reward;
}
