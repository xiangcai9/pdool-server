package com.pdool.common.dict;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CardUpLevelVo {
    private int id;
    private String cost;
    private String cardTreeAdd;
}
