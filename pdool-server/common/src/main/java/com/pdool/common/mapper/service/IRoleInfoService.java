package com.pdool.common.mapper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pdool.common.entity.RoleInfoEntity;

public interface IRoleInfoService extends IService<RoleInfoEntity> {
}

