package com.pdool.common.mapper.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pdool.common.entity.CardPieceEntity;
import com.pdool.common.mapper.mapper.CardPieceMapper;
import com.pdool.common.mapper.service.ICardPieceService;
import org.springframework.stereotype.Service;

@Service
public class CardPieceDaoImpl extends ServiceImpl<CardPieceMapper, CardPieceEntity> implements ICardPieceService {

}
