package com.pdool.common.exception;

public class ErrorException extends RuntimeException {

    private final String errorCode;
    private final String msg;

    public ErrorException(String errorCode) {
        this(errorCode, null, null);
    }

    public ErrorException(String errorCode, String msg) {
        this(errorCode, msg, null);
    }

    public ErrorException(String errorCode, String msg, Throwable cause) {
        super("errorCode:[" + errorCode + "],msg:[" + msg + "]", cause);
        this.errorCode = errorCode;
        this.msg = msg;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getMsg() {
        return msg;
    }
}
