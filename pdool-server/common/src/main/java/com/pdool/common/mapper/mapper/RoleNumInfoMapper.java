package com.pdool.common.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdool.common.entity.RoleNumInfoEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoleNumInfoMapper extends BaseMapper<RoleNumInfoEntity> {
}
