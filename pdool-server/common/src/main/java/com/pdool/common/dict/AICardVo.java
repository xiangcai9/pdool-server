package com.pdool.common.dict;

import cn.hutool.core.lang.Pair;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AICardVo {
    private int id;
    private List<Integer> cardList = new ArrayList<>();
    private Pair<Integer,Integer> battleNum;
}
