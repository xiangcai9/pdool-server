package com.pdool.common.constants;

public class MsgId {


    public static final int ERROR = 100;
    public static final int LOGIN_OUT = 1000;
    public static final int HB = 1001;


    public static final int CARD_MAKE_GROUP = 2000;
    public static final int UPDATE_CARD_GROUP = 2001;
    public static final int GROUP_INFO = 2002;

    public static final int GROUP_RENAME = 2003;
    public static final int CARD_UP_LV = 2100;
    public static final int CARD_TREE_GET_REWARD = 2101;
    public static final int CARD_TREE_EXP_ADD = 2102;
    public static final int QUERY_CARD_PIECE = 2103;

    public static final int QUERY_SCORE_RANK = 2200;






    public static final int STC_CREATE_ROOM_SUCCESS = 3000;
    public static final int MATCH = 3001;


    public static final int PUSH_CARD_INFO = 6000;
    public static final int PUSH_ROLE_INFO = 6001;
    public static final int PUSH_ROLE_NUM_INFO = 6002;
    public static final int PUSH_GUIDE_INFO = 6003;
    public static final int SET_GUIDE_INFO = 6004;
    public static final int PUSH_SINGLE_NUM_TYPE = 6005;





    public static final int READY_FIGHT = 13001;
    public static final int PUT_CARD = 13002;
    public static final int QUIT_FIGHT = 13003;
    public static final int PUSH_HAND_CARD_ADD = 13004;
    public static final int PUSH_FIGHT_RESULT = 13005;
    public static final int PUSH_ENERGY_CHANGE = 13006;



    public static final int PUSH_CARD_FIGHT_CHANGE = 13007;
    public static final int PUSH_AREA_FIGHT_CHANGE = 13008;
    public static final int PUSH_CARD_BACK_HAND = 13009;
    public static final int PUSH_SHOW_CARD = 13010;
    public static final int ACTIVE_AREA_EFFECT = 13011;
    public static final int NEXT_ROUND = 13012;
    public static final int START_ROUND = 13013;
    public static final int OTHER_HAND_COUNT = 13014;
    public static final int PUSH_CARD_FIGHT_INFO = 13015;
}
