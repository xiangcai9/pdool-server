package com.pdool.common.dict;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CardEffectVo {
    private int effectId;

    //  效果生效回合
    private int roundPos;
    //  生效的特定回合
    private Integer activeRound;


    //  敌我类型
    private int campType;
    //  战场类型
    private int areaType;
    //  目标详细类型
    private String[] targetCondition;

    // action（效果作用）
    private int exAction;
    //  生效对象
    private int actionTarget;
    //  action对应参数
    private Integer actionNum;
    //
    private int actionPhase;

}
