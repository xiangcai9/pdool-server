package com.pdool.common.dict;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PropVo {
    private Integer  id;
    private String name;
    private int itemType;
    private int quality;
    private int bag;
    private String price;


}
