// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: common.proto

package com.pdool.common.msg.common;

public final class Common {
  private Common() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_proto_NullReq_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_proto_NullReq_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_proto_NullResp_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_proto_NullResp_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_proto_ErrorMsgResp_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_proto_ErrorMsgResp_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_proto_HBReq_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_proto_HBReq_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\014common.proto\022\005proto\"\t\n\007NullReq\"\n\n\010Null" +
      "Resp\"<\n\014ErrorMsgResp\022\017\n\007errCode\030\001 \001(\t\022\013\n" +
      "\003msg\030\002 \001(\t\022\016\n\006reason\030\003 \001(\t\"\007\n\005HBReqB\037\n\033c" +
      "om.pdool.common.msg.commonP\001b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_proto_NullReq_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_proto_NullReq_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_proto_NullReq_descriptor,
        new java.lang.String[] { });
    internal_static_proto_NullResp_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_proto_NullResp_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_proto_NullResp_descriptor,
        new java.lang.String[] { });
    internal_static_proto_ErrorMsgResp_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_proto_ErrorMsgResp_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_proto_ErrorMsgResp_descriptor,
        new java.lang.String[] { "ErrCode", "Msg", "Reason", });
    internal_static_proto_HBReq_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_proto_HBReq_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_proto_HBReq_descriptor,
        new java.lang.String[] { });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
