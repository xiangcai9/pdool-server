package com.pdool.common.constants;

public class Constant {


    public static final int ROOM_MSG_GAP = 10000;
    //每个玩家最大未处理掉的消息队列数
    public static final int USER_MAX_MSG_QUEUE_NUM = 100;


    // 获取服务器的cpu个数
    private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();// 获取cpu个数

    public static final String LOGIC_THREAD_POOL_NAME = "logic_pool";
    public static final int LOGIC_THREAD_POOL_QUEUE_SIZE = 100000;
    public static final int LOGIC_THREAD_POOL_CORE_SIZE = 100;
    public static final int LOGIC_THREAD_POOL_MAX_SIZE = 100;
    public static final int LOGIC_THREAD_POOL_KEEP_LIVE = 60;


    public static final String SLOW_THREAD_POOL_NAME = "slow_pool";

    public static final int SLOW_THREAD_POOL_QUEUE_SIZE = 100000;
    public static final int SLOW_THREAD_POOL_CORE_SIZE = 20;
    public static final int SLOW_THREAD_POOL_MAX_SIZE = 40;
    public static final int SLOW_THREAD_POOL_KEEP_LIVE = 60;
    /**
     * "," - and分割符
     **/
    public static final String SPLIT_AND = "&";

    /**
     * "," - 逗号分割符
     **/
    public static final String SPLIT_COMMA = ",";
    /**
     * ";" - 分号分割符
     **/
    public static final String SPLIT_SEMICOLON = ";";
    /**
     * "_" - 下划线分隔符
     **/
    public static final String SPLIT_LINE = "_";
    /**
     * "|" - 竖线分隔符
     **/
    public static final String SPLIT_VERTICAL_BAR = "\\|";
    /**
     * "$" - 美元分隔符
     **/
    public static final String SPLIT_DOLLAR = "\\$";
    /**
     * "#" - 井号分隔符
     */
    public static final String SPLIT_SHARP = "#";
    /**
     * "-" - 中划线分隔符
     */
    public static final String SPLIT_CROSS_LINE = "\\-";
    /**
     * ":" - 冒号分隔符
     */
    public static final String SPLIT_COLON = ":";
    /**
     * '!'感叹号
     */
    public static final String EXCLAMATION = "!";
    /**
     * '$'美元符号
     */
    public static final String DOLLAR = "$";
    /**
     * '|'竖线
     */
    public static final String VERTICAL_BAR = "|";
    /**
     * '-'中划线符号
     */
    public static final String CROSS_LINE = "-";

    public static final int MATCH_GROUP = 10;
    public static final int MATCH_TIME_OUT = 5;

    public static final int DEFAULT_CARD_GROUP = 1;

    public static final String CARD_TREE_EXP = "3";

    //  表base_config 中id配置
    //  初始卡组中的卡牌
    public static final String PROP_INIT_CARD_GROUP = "1";
    //  卡牌的经验范围
    public static final String PROP_MAX_CARD_EXP = "2";
    //  除drop表中的通用奖励外，每一局战斗选取1张卡组张的牌奖励额外对应卡牌升级道具(废弃)
    public static final String PROP_FIGHT_DROP_NUM = "3";
    //  每个区域能放的最多的牌
    public static final String PROP_AREA_MAX_CARD_NUM = "4";
    //自动出牌时间 30s
    public static final String PROP_AUTO_PUT_CARD_SEC = "5";
    // 有几个战斗区域
    public static final String PROP_AREA_COUNT = "6";
    //  卡组中最多的卡牌数量
    public static final String PROP_GROUP_MAX_COUNT = "7";
    //  初始手牌数量
    public static final String PROP_INIT_HAND_CARD_COUNT = "8";

    //  召唤的卡牌列表
    public static final String PROP_CALL_LIST = "9";
    //  最大回合数
    public static final String PROP_MAX_ROUND = "10";
    //  战斗掉落
    public static final String PROP_FIGHT_DROP = "11";
    //  创角送的卡
    public static final String PROP_CREATE_ROLE_CARD = "12";
    // 进入战场需要的卡组满足的数量
    public static final String PROP_FIGHT_GROUP_NUM = "13";
    // 获得积分
    public static final String PROP_FIGHT_SCORE = "14";








    public static final int TIME_OUT_LOGOUT_MIN = 2;





    //  卡牌效果表生效对象
    public static final int ACTION_TARGET_1 = 1;
    public static final int ACTION_TARGET_2 = 2;
    public static final int ACTION_TARGET_3 = 3;
    public static final int ACTION_TARGET_4 = 4;
    public static final int ACTION_TARGET_5 = 5;
    //  卡牌效果表 效果作用
    public static final int EX_ACTION_TARGET_1 = 1;
    public static final int EX_ACTION_TARGET_2 = 2;
    public static final int EX_ACTION_TARGET_3 = 3;
    public static final int EX_ACTION_TARGET_4 = 4;
    public static final int EX_ACTION_TARGET_5 = 5;
    public static final int EX_ACTION_TARGET_6 = 6;
    public static final int EX_ACTION_TARGET_7 = 7;
    public static final int EX_ACTION_TARGET_8 = 8;
    public static final int EX_ACTION_TARGET_10 = 10;
    public static final int EX_ACTION_TARGET_11 = 11;
    public static final int EX_ACTION_TARGET_12 = 12;
    public static final int EX_ACTION_TARGET_13 = 13;

    public static final int EX_ACTION_TARGET_14 = 14;
    public static final int EX_ACTION_TARGET_15 = 15;


    public static final int EX_ACTION_TARGET_21 = 21;
    public static final int EX_ACTION_TARGET_22 = 22;
    public static final int EX_ACTION_TARGET_23 = 23;
    public static final int EX_ACTION_TARGET_24 = 24;


    //  卡牌效果表 效果生效回合
    public static final int ROUND_POS_1 = 1;
    public static final int ROUND_POS_2 = 2;
    public static final int ROUND_POS_3 = 3;
    public static final int ROUND_POS_4 = 4;
    public static final int ROUND_POS_7 = 7;



    //  目标详细类型
    public static final int TARGET_DETAIL_TYPE_1 = 1;
    public static final int TARGET_DETAIL_TYPE_2 = 2;
    public static final int TARGET_DETAIL_TYPE_3 = 3;
    public static final int TARGET_DETAIL_TYPE_4 = 4;

    //  区域类型
    //  候选池
    public static final int AREA_POOL = -2;
    //  手牌
    public static final int AREA_HAND = -1;







    //  0-当前
    public static final int AREA_TYPE_0 = 0;
    //  1-相邻
    public static final int AREA_TYPE_1 = 1;
    //  2左侧区域
    public static final int AREA_TYPE_2 = 2;
    //  3右侧区域
    public static final int AREA_TYPE_3 = 3;
    //  4全部区域（不包括手牌区域）
    public static final int AREA_TYPE_4 = 4;
    //  5除当前之外的所有区域
    public static final int AREA_TYPE_5 = 5;
    // 6 手牌区域
    public static final int AREA_TYPE_6 = 6;
    //  7 手牌区域+ 战场区域（全部卡牌）
    public static final int AREA_TYPE_7 = 7;


    //  redis key game在线数量
    public static final String GAME_ONLINE_COUNT = "online_count";
}
