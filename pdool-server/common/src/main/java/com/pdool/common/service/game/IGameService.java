package com.pdool.common.service.game;

public interface IGameService {
    void trans2Client(long roleId,byte[] buffer);
    void trans2Game(Object msg);
    String sayHello();
}
