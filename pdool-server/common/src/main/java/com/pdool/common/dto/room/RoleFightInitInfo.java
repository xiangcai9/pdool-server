package com.pdool.common.dto.room;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class RoleFightInitInfo implements Serializable {
    long roleId;
    int pos;
    List<Integer> cardList = new ArrayList<>();
}
