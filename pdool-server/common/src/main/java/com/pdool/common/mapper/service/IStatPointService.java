package com.pdool.common.mapper.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pdool.common.entity.StatPointEntity;

public interface IStatPointService extends IService<StatPointEntity> {
}

