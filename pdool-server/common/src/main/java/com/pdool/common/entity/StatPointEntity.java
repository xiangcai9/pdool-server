package com.pdool.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("stat_point")
public class StatPointEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Long roleId;
    private String statDate;
    private Integer type;
    private Long value;
    private String extend;
    private Date saveTime;

}
