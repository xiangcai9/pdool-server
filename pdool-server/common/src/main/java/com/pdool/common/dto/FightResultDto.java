package com.pdool.common.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Data
public class FightResultDto implements Serializable {
    private String roomId;
    private List<Long> roleList;
    private Set<Long> winnerSet = new HashSet<>(); //  胜者组
    private Set<Long> loserSet = new HashSet<>(); //  败者着
    private int reason = 1;//   0 正常结束 1 退出战斗
}
