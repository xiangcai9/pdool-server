package com.pdool.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("card_piece")
@Data
public class CardPieceEntity {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private Long roleId;
    private int pieceId;
    private int num;

}
