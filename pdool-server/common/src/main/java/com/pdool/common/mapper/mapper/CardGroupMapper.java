package com.pdool.common.mapper.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pdool.common.entity.CardGroupEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CardGroupMapper extends BaseMapper<CardGroupEntity> {
}
