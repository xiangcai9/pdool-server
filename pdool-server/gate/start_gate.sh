 docker run -itd \
  -e ENV=test \
  -p 18080:8080 \
  --name gate \
  -v /data/excel:/excel \
  -v /home/docker/game:/app/log \
  --restart=always gate:0.0.1-SNAPSHOT
