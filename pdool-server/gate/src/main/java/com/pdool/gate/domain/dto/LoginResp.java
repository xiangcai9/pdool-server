package com.pdool.gate.domain.dto;

import lombok.Data;

/**
 * 登录相应
 */
@Data
public class LoginResp {
    private String gameUri;
    private String token;
    private String error;
}
