package com.pdool.gate.cache;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "excel")
@Data
public class ExcelConfig {
    private String excelPath;

}
