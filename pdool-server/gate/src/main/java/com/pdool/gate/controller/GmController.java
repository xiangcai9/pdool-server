package com.pdool.gate.controller;

import com.pdool.gate.service.GmService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * gm
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/gm")
public class GmController {
    @Resource
    GmService gmService;

    @GetMapping("/addBlack/{roleId}")
    public String addBlack(@PathVariable Long roleId) {
        boolean b = gmService.addBlack(roleId);
        if (b) {
            return roleId + "加入成功";
        } else {
            return "加入失败";
        }
    }
}
