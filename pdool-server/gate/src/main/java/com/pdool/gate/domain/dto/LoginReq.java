package com.pdool.gate.domain.dto;

import lombok.Data;

/**
 * 登录请求
 */
@Data
public class LoginReq {
    private String userName;
    private String pwd;
}
