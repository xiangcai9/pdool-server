package com.pdool.gate.domain.dto;

import lombok.Data;

/**
 * 创角请求对象
 */
@Data
public class CreateRoleReq {
    private String userName;
    private String pwd;
    private String head;
}
