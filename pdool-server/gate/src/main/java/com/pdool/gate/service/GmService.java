package com.pdool.gate.service;

import com.pdool.common.mapper.mapper.BlackMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@Slf4j
public class GmService {
    @Autowired
    BlackMapper blackMapper;


    public boolean addBlack(Long roleId) {
        return blackMapper.insertOne(roleId,new Date());
    }


    public boolean isInBlack(Long roleId) {
        return blackMapper.isExist(roleId) > 0;
    }
}
