package com.pdool.gate.cache.dict;

import com.pdool.common.constants.Constant;
import com.pdool.common.dict.CardInfoConfigVo;
import com.pdool.gate.cache.AbsConfigCache;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 卡牌配置
 */
@Component
public class CardConfig extends AbsConfigCache<Integer, CardInfoConfigVo> {
    public CardConfig() {
        super("卡牌表_card_info.xlsx",3);
    }

    @Override
    protected CardInfoConfigVo convert2Value(Map<Integer, String> rowData) {
        int id = Integer.parseInt(rowData.get(1));
        String name = rowData.get(2);
        int type = Integer.parseInt(rowData.get(5));
        int cost = Integer.parseInt(rowData.get(6));
        int act = Integer.parseInt(rowData.get(7));
        int lv = Integer.parseInt(rowData.get(8));
        String levelCost = rowData.get(9);
        CardInfoConfigVo.CardInfoConfigVoBuilder builder = CardInfoConfigVo.builder();
        if (StringUtils.hasText(rowData.get(11))){
            int effect = Integer.parseInt(rowData.get(11));
            builder.effect(effect);
        }
        Map<Integer, String> lvCostMap = new HashMap<>();
        String[] split = levelCost.split(Constant.SPLIT_COMMA);
        for (String s : split) {
            String[] split1 = s.split(Constant.SPLIT_SHARP);
            int lvId = Integer.parseInt(split1[0]);
            lvCostMap.put(lvId, split1[1]);
        }

//        int limit = Integer.parseInt(rowData.get(13));
        CardInfoConfigVo build = builder.id(id)
                .name(name)
                .cardType(type)
                .cost(cost)
                .fight(act)
                .lv(lv)
                .levelCost(lvCostMap)
//                .limit(limit)
                .build();

        return build;
    }

    @Override
    protected Integer convert2Key(Map<Integer, String> rowData) {
        String s = rowData.get(1);
        return Integer.valueOf(s);
    }
}
