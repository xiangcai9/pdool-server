#/bin/bash
PROFILE=$ENV
if [[ "$PROFILE" == "dev" ]];then
	echo "dev"
	java  -Duser.timezone=GMT+08 -jar gate.jar --spring.profiles.active=dev
elif [[ "$PROFILE" == "test" ]];then
	echo "test"
	java  -Duser.timezone=GMT+08  -jar gate.jar --spring.profiles.active=test
elif [[ "$PROFILE" == "prod" ]];then
	echo "prod"
	java -Duser.timezone=GMT+08 -jar gate.jar --spring.profiles.active=prod
fi