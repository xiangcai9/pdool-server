package com.pdool.tool.create;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.pdool.common.constants.Constant;
import com.pdool.common.entity.CardGroupEntity;
import com.pdool.common.entity.CardInfoEntity;
import com.pdool.common.entity.RoleInfoEntity;
import com.pdool.common.mapper.impl.CardGroupDaoImpl;
import com.pdool.common.mapper.impl.CardInfoDaoImpl;
import com.pdool.common.mapper.impl.RoleInfoDaoImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class CreateRoleHandler implements ApplicationRunner {
    @Resource
    RoleInfoDaoImpl roleInfoDao;

    @Autowired
    CardInfoDaoImpl cardInfoMapper;
    @Autowired
    CardGroupDaoImpl cardGroupMapper;

    public void createRole(String userName, Long maxRoleId) {
        String pwd = RandomUtil.randomString(9);
        Date date = new Date();
        RoleInfoEntity roleInfoEntity = new RoleInfoEntity();
        roleInfoEntity.setName("测试" + maxRoleId);
        roleInfoEntity.setRoleId(maxRoleId);
        roleInfoEntity.setPwd(pwd);
        roleInfoEntity.setRoleType(2);
        roleInfoEntity.setHead("head");
        roleInfoEntity.setCreateTime(date);
        roleInfoEntity.setUpdateTime(date);
        roleInfoDao.save(roleInfoEntity);

        String initCard = "1,2,3,4,5,6,7,8,9,12,13,14,15,17,18,19,20,21,25,37,38,39,42,44,45,46,47,53,57,62,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89";

        String[] createRoleCardArr = initCard.split(Constant.SPLIT_COMMA);
        ArrayList<CardInfoEntity> initCardList = new ArrayList<>();

        for (String s : createRoleCardArr) {
            int id = Integer.parseInt(s);
            CardInfoEntity cardInfoEntity = new CardInfoEntity();
            cardInfoEntity.setCardId(id);
            cardInfoEntity.setRoleId(roleInfoEntity.getRoleId());
            cardInfoEntity.setLv(1);
            initCardList.add(cardInfoEntity);
        }
        cardInfoMapper.saveBatch(initCardList);

        CardGroupEntity cardGroupEntity = new CardGroupEntity();
        cardGroupEntity.setRoleId(maxRoleId);
        cardGroupEntity.setGroupIndex(Constant.DEFAULT_CARD_GROUP);
        cardGroupEntity.setCardGroup("2,4,5,7,8,18,19,20,21,37,45,46");
        cardGroupMapper.save(cardGroupEntity);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
//        ExcelReader reader = ExcelUtil.getReader("D:\\game\\pdool\\副本迷你世界卡牌内侧第一批名单.xlsx");
//        List<List<Object>> readAll = reader.read();
//        for(int i = 0;i<readAll.size();i++){
////            runThread("thread" + i,readAll,i*500,(i+1)*500);
//            List<Object> objects = readAll.get(i);
//            long roleId = Integer.parseInt(objects.get(1).toString());
//            check(roleId);
//        }
        String s = "1428343206,1799285783,1822206617,88390099,1824910611,1271868243";
        String[] split = s.split(",");
        for (String s1 : split) {
            check(Integer.parseInt(s1));
        }
//        runThread("thread1 ",readAll,0,1000);
//        runThread("thread2 ",readAll,1000,2000);
//        runThread("thread3 ",readAll,2000,3000);
//        runThread("thread4 ",readAll,3000,4000);
//        runThread("thread5 ",readAll,4000,6000);

    log.error("check执行完毕");
    }


    public void runThread(String threadName,List<List<Object>> readAll, int beginIndex, int endIndex) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = beginIndex; i < endIndex && i < readAll.size(); i++) {
                    List<Object> objects = readAll.get(i);
                    long roleId = Integer.parseInt(objects.get(1).toString());
                    createRole("", roleId);
                    System.out.println(threadName + "   index  " + i + "   roleId  " + roleId);
                }
            }
        }).start();
        log.error("create 执行完毕 threadName {}",threadName);
    }

    public void check(long roleId) {
        QueryWrapper<RoleInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("role_id",roleId);
        long count = roleInfoDao.count(wrapper);
        if (count == 0){
            createRole("",roleId);
            System.out.println("create role  "+ roleId);
        }
    }
    public static void main(String[] args) {
        String s1 = RandomUtil.randomString(9);
        System.out.println(s1);
    }
}
